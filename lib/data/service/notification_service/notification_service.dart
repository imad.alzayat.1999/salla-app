import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class NotificationService{
  String? userToken = LocalStorage.getData(key: "token");

  Future<List<dynamic>> getNotifications({
    required String language,
  }) async {
    try {
      final response = await DioHelper.dio.get(
        notificationsEndPoint,
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      print(response.data);
      return response.data["data"]["data"];
    } catch (e) {
      print(e);
      return [];
    }
  }
}