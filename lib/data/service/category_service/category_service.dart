import 'package:dio/dio.dart';
import 'package:shop_application/data/repository/category_repository/category_repository.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class CategoryService{
  String? userToken = LocalStorage.getData(key: "token");

  Future<List<dynamic>> getCategories({
    required String language,
  }) async {
    try {
      final response = await DioHelper.dio.get(
        categoryEndPoint,
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      return response.data["data"]["data"];
    } catch (e) {
      print(e);
      return [];
    }
  }

}
