import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/end_points.dart';

import '../../../shared/network/dio_helper.dart';

class LoginService {
  Future<dynamic> loginAccount({
    required String language,
    required String email,
    required String password,
  }) async {
    try {
      final response = await DioHelper.dio.post(
        loginEndPoint,
        data: {
          "email": email,
          "password": password,
        },
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "lang": language,
          },
        ),
      );
      print(response.data);
      return response.data;
    } catch (e) {
      print(e);
      return null;
    }
  }
}