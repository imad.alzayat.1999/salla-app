import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';

class RegisterService {
  Future<dynamic> registerAccount({
    required String language,
    required String name,
    required String email,
    required String password,
    required String phone,
  }) async {
    try {
      final response = await DioHelper.dio.post(
        signupEndPoint,
        data: {
          "name": name,
          "email": email,
          "password": password,
          "phone": phone,
        },
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "lang": language,
          },
        ),
      );
      print(response.data);
      return response.data;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
