import 'package:shop_application/models/favorites/favorites_model.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../../../shared/local_storage/local_storage.dart';

class FavoriteService {
  static final FavoriteService instance = FavoriteService._init();

  static Database? _database;

  FavoriteService._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('${ConstsManager.tableFavorites}.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY';
    const textType = 'TEXT';
    const integerType = 'INTEGER';
    const realType = 'REAL';

    await db.execute('''
CREATE TABLE ${ConstsManager.tableFavorites} ( 
  ${FavoriteFields.id} $idType, 
  ${FavoriteFields.productId} $integerType,
  ${FavoriteFields.productName} $textType,
  ${FavoriteFields.productImage} $textType,
  ${FavoriteFields.productPrice} $realType,
  ${FavoriteFields.productDescription} $textType,
  ${FavoriteFields.productOldPrice} $realType,
  ${FavoriteFields.productDiscount} $integerType,
  ${FavoriteFields.userToken} $textType
  )
''');
  }

  Future<FavoritesModel> create(FavoritesModel favoritesModel) async {
    final db = await instance.database;
    favoritesModel.id =
        await db.insert(ConstsManager.tableFavorites, favoritesModel.toJson());
    return favoritesModel;
  }

  Future<List<FavoritesModel>> readAllNotes() async {
    String? userToken = LocalStorage.getData(key: "token");
    print("user token for favorites ${userToken}");
    final db = await instance.database;
    final result = await db.query(ConstsManager.tableFavorites,
        where: "user_token = ?", whereArgs: [userToken!]);
    return result.map((json) => FavoritesModel.fromJson(json)).toList();
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      ConstsManager.tableFavorites,
      where: '${FavoriteFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }

  Future deleteTable() async {
    final db = await instance.database;

    db.execute("delete from " + ConstsManager.tableFavorites);
  }
}
