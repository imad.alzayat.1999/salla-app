import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

import '../../../shared/network/dio_helper.dart';

class ProfileService {
  String? userToken = LocalStorage.getData(key: "token");

  Future<dynamic> getProfileInformation({
    required String language,
  }) async {
    try {
      final response = await DioHelper.dio.get(
        profileEndPoint,
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      print(response.data["data"]);
      return response.data["data"];
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<dynamic> updateProfileInformation({
    required String language,
    required String name,
    required String phone,
    required String email,
    required String password,
    required String image,
  }) async {
    try {
      final response = await DioHelper.dio.put(
        updateProfileEndPoint,
        data: {
          "name": name,
          "email": email,
          "phone": phone,
          "password": password,
          "image": image,
        },
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      print(response);
      return response.data["data"];
    } catch (e) {
      print(e);
      return null;
    }
  }
}
