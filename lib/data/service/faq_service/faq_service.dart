import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class FAQService{
  String? userToken = LocalStorage.getData(key: "token");

  Future<List<dynamic>> getFAQs({
    required String language,
  }) async {
    try {
      final response = await DioHelper.dio.get(
        faqEndPoint,
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      return response.data["data"]["data"];
    } catch (e) {
      print(e);
      return [];
    }
  }
}
