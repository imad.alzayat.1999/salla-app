import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class SearchService{
  String? userToken = LocalStorage.getData(key: "token");

  Future<List<dynamic>> getSearchedProducts({
    required String language,
    required String inputText
  }) async {
    try {
      final response = await DioHelper.dio.post(
        searchEndPoint,
        data: {
          "text" : inputText
        },
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      return response.data["data"]["data"];
    } catch (e) {
      print(e);
      return [];
    }
  }
}
