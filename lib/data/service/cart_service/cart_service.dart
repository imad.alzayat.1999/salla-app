import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../../../models/cart/cart_model.dart';

class CartService {
  static final CartService instance = CartService._init();

  static Database? _database;

  CartService._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('${ConstsManager.tableCart}.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY';
    const textType = 'TEXT';
    const integerType = 'INTEGER';
    const realType = 'REAL';

    await db.execute('''
CREATE TABLE ${ConstsManager.tableCart} ( 
  ${CartFields.id} $idType, 
  ${CartFields.quantity} $integerType,
  ${CartFields.productId} $integerType,
  ${CartFields.productName} $textType,
  ${CartFields.productImage} $textType,
  ${CartFields.productPrice} $realType,
  ${CartFields.productDescription} $textType,
  ${CartFields.productOldPrice} $realType,
  ${CartFields.productDiscount} $integerType,
  ${CartFields.userToken} $textType
  )
''');
  }

  Future<CartModel> create(CartModel cartModel) async {
    final db = await instance.database;
    cartModel.id = await db.insert(ConstsManager.tableCart, cartModel.toJson());
    return cartModel;
  }

  Future<List<CartModel>> readAllNotes() async {
    String? userToken = LocalStorage.getData(key: "token");
    print("user token for cart is ${userToken}");
    final db = await instance.database;
    final result = await db.query(ConstsManager.tableCart,
        where: "user_token = ?", whereArgs: [userToken!]);
    print(result);
    return result.map((json) => CartModel.fromJson(json)).toList();
  }

  Future<int> update(CartModel cartModel) async {
    final db = await instance.database;

    return await db.update(
      ConstsManager.tableCart,
      cartModel.toJson(),
      where: '${CartFields.id} = ?',
      whereArgs: [cartModel.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      ConstsManager.tableCart,
      where: '${CartFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }

  Future deleteTable() async {
    final db = await instance.database;

    db.execute("delete from " + ConstsManager.tableCart);
  }
}
