import 'package:dio/dio.dart';
import 'package:dio/src/response.dart';
import 'package:shop_application/data/repository/home_repository/home_repository.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class HomeService{
  String? userToken = LocalStorage.getData(key: "token");
  Future<dynamic> getHomeDataService({required String language}) async{
    try {
      final response = await DioHelper.dio.get(
        homeEndPoint,
        options: Options(
          headers: {
            "lang": language,
            "Content-Type": "application/json",
            "Authorization": userToken!,
          },
        ),
      );
      return response.data["data"];
    } catch (e) {
      print(e);
      return null;
    }
  }
}