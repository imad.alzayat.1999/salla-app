import 'package:dio/dio.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/network/end_points.dart';

import '../../../shared/network/dio_helper.dart';

class ChangePasswordService{
  String? userToken = LocalStorage.getData(key: "token");
  Future<dynamic> changePassword({
    required String language,
    required String currentPassword,
    required String newPassword,
  }) async {
    try {
      final response = await DioHelper.dio.post(
        changePasswordEndPoint,
        data: {
          "current_password": currentPassword,
          "new_password": newPassword,
        },
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "lang": language,
            "Authorization": userToken!
          },
        ),
      );
      print(response.data);
      return response.data;
    } catch (e) {
      print(e);
      return null;
    }
  }
}