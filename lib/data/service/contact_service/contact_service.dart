import 'package:dio/dio.dart';
import 'package:shop_application/shared/network/end_points.dart';

import '../../../shared/network/dio_helper.dart';
import '../../../shared/local_storage/local_storage.dart';

class ContactService{
  String? userToken = LocalStorage.getData(key: "token");

  Future<List<dynamic>> getContacts({
    required String language,
  }) async {
    try {
      final response = await DioHelper.dio.get(
        contactEndPoint,
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": userToken!,
            "lang": language,
          },
        ),
      );
      return response.data["data"]["data"];
    } catch (e) {
      print(e);
      return [];
    }
  }
}