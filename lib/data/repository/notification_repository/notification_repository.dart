import 'package:shop_application/data/service/notification_service/notification_service.dart';
import 'package:shop_application/models/notification/notification_model.dart';

class NotificationRepository{
  final NotificationService notificationService;

  NotificationRepository(this.notificationService);

  Future<List<NotificationItem>> getNotificationsRepository({required String language}) async{
    final notificationsResponse = await notificationService.getNotifications(language: language);
    return notificationsResponse.map((e) => NotificationItem.fromJson(e)).toList();
  }
}