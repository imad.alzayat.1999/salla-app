import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shop_application/data/service/authentication_service/register_service.dart';

class RegisterRepository {
  final RegisterService registerService;

  RegisterRepository({required this.registerService});

  Future<dynamic> registerAccountRepository({
    required String email,
    required String password,
    required String phone,
    required String name,
    required String language,
  }) async {
    final registerResponse = await registerService.registerAccount(
        language: language,
        name: name,
        email: email,
        password: password,
        phone: phone);
    return registerResponse;
  }
}
