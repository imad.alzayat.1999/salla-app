import 'package:shop_application/data/service/authentication_service/login_service.dart';
import 'package:shop_application/models/authentication/login_model.dart';

class LoginRepository {
  final LoginService loginService;

  LoginRepository({required this.loginService});

  Future<dynamic> loginAccountRepository(
      {required String email,
      required String password,
      required String language}) async {
    final loginResponse = await loginService.loginAccount(
        language: language, email: email, password: password);
    return loginResponse;
  }
}
