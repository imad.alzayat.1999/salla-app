import 'package:shop_application/data/service/cart_service/cart_service.dart';
import 'package:shop_application/models/cart/cart_model.dart';

class CartRepository{
  final CartService cartService;

  CartRepository({required this.cartService});

  Future<CartModel> insertProductIntoCart({required CartModel cartModel}) async{
    final insertToCartResponse = await cartService.create(cartModel);
    return insertToCartResponse;
  }

  Future<List<CartModel>> getAllProductsFromCart() async{
    final getAllProductsFromCartResponse = await cartService.readAllNotes();
    print(getAllProductsFromCartResponse);
    return getAllProductsFromCartResponse;
  }

  Future<int> updateProductInCart({required CartModel cartModel}) async{
    final updateProductInCartResponse = await cartService.update(cartModel);
    return updateProductInCartResponse;
  }

  Future<int> deleteProductFromCart({required int id}) async{
    final deleteProductFromCartResponse = await cartService.delete(id);
    return deleteProductFromCartResponse;
  }
  Future deleteCartTable() async{
    final deleteCartTableResponse = await cartService.deleteTable();
    return deleteCartTableResponse;
  }
}