import 'package:dio/dio.dart';
import 'package:shop_application/data/service/faq_service/faq_service.dart';
import 'package:shop_application/models/faq/faq_model.dart';

class FAQRepository{
  final FAQService faqService;

  FAQRepository({required this.faqService});

  Future<List<FAQ>> getFaqRepository({required String language}) async{
    final faqResponse = await faqService.getFAQs(language: language);
    return faqResponse.map((e) => FAQ.fromJson(e)).toList();
  }
}