import 'package:shop_application/data/service/change_password_service/change_password_service.dart';
import 'package:shop_application/models/change_password/change_password_model.dart';

class ChangePasswordRepository {
  final ChangePasswordService changePasswordService;

  ChangePasswordRepository({required this.changePasswordService});

  Future<ChangePasswordModel> changePassword({
    required String language,
    required String currentPassword,
    required String newPassword,
  }) async {
    final changePasswordResponse = await changePasswordService.changePassword(
        language: language,
        currentPassword: currentPassword,
        newPassword: newPassword);
    return ChangePasswordModel.fromJson(changePasswordResponse);
  }
}
