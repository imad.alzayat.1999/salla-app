import 'package:shop_application/data/service/category_service/category_service.dart';
import 'package:shop_application/models/category/category_model.dart';

class CategoryRepository{
  final CategoryService categoryService;

  CategoryRepository(this.categoryService);

  Future<List<Category>> getCategoriesRepository({required String language}) async{
    final categoriesResponse = await categoryService.getCategories(language: language);
    return categoriesResponse.map((e) => Category.fromJson(e)).toList();
  }
}