import 'package:shop_application/data/service/favorite_service/favorite_service.dart';
import 'package:shop_application/models/favorites/favorites_model.dart';

class FavoriteRepository{
  final FavoriteService favoriteService;

  FavoriteRepository({required this.favoriteService});

  Future<FavoritesModel> insertProductIntoFavorites({required FavoritesModel favoritesModel}) async{
    final insertToFavoritesResponse = await favoriteService.create(favoritesModel);
    return insertToFavoritesResponse;
  }

  Future<List<FavoritesModel>> getAllProductsFromFavorites() async{
    final getAllProductsFromFavoritesResponse = await favoriteService.readAllNotes();
    return getAllProductsFromFavoritesResponse;
  }

  Future<int> deleteProductFromFavorites({required int id}) async{
    final deleteProductFromFavoritesResponse = await favoriteService.delete(id);
    return deleteProductFromFavoritesResponse;
  }
  Future deleteFavoritesTable() async{
    final deleteFavoritesTableResponse = await favoriteService.deleteTable();
    return deleteFavoritesTableResponse;
  }
}