import 'package:shop_application/data/service/home_service/home_service.dart';

class HomeRepository{
  final HomeService homeService;

  HomeRepository({required this.homeService});

  Future<dynamic> getHomeDataRepository({required String language}) async{
    final homeDataResponse = await homeService.getHomeDataService(language: language);
    return homeDataResponse;
  }

}