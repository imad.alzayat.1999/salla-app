import 'package:shop_application/data/service/profile_service/profile_service.dart';

import '../../../models/profile/profile_model.dart';

class ProfileRepository {
  final ProfileService profileService;

  ProfileRepository({required this.profileService});

  Future<Profile> getProfileInformation({required String language}) async {
    final fetchProfileInfoResponse =
        await profileService.getProfileInformation(language: language);
    return Profile.fromJson(fetchProfileInfoResponse);
  }

  Future<Profile> updateProfileInformation({
    required String language,
    required String name,
    required String phone,
    required String email,
    required String password,
    required String image,
  }) async {
    final updateProfileResponse = await profileService.updateProfileInformation(
      language: language,
      name: name,
      phone: phone,
      email: email,
      password: password,
      image: image,
    );
    return Profile.fromJson(updateProfileResponse);
  }
}
