import 'package:shop_application/data/service/search_service/search_service.dart';
import 'package:shop_application/models/search/search_model.dart';

class SearchRepository {
  final SearchService searchService;

  SearchRepository({required this.searchService});

  Future<List<SearchedProduct>> getSearchedProductsRepository({
    required String language,
    required String inputText,
  }) async {
    final searchedProductsResponse = await searchService.getSearchedProducts(
        language: language, inputText: inputText);
    return searchedProductsResponse
        .map((e) => SearchedProduct.fromJson(e))
        .toList();
  }
}
