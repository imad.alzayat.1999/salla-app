import 'package:shop_application/data/service/contact_service/contact_service.dart';
import 'package:shop_application/models/contact/contact_model.dart';

class ContactRepository{
  final ContactService contactService;

  ContactRepository({required this.contactService});

  Future<List<Contact>> getContactRepository({required String language}) async{
    final contactResponse = await contactService.getContacts(language: language);
    return contactResponse.map((e) => Contact.fromJson(e)).toList();
  }
}