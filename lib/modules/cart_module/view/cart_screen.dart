import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/cart_repository/cart_repository.dart';
import 'package:shop_application/data/service/cart_service/cart_service.dart';
import 'package:shop_application/models/cart/cart_model.dart';
import 'package:shop_application/modules/cart_module/components/cart_item.dart';
import 'package:shop_application/modules/cart_module/components/dismissible_component.dart';
import 'package:shop_application/modules/cart_module/components/total_price_and_count.dart';
import 'package:shop_application/modules/cart_module/logic/controller/cart_controller.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/base/base_toast.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../../shared/base_application_page/base_application_page.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';
import '../../../shared/widgets/widget_states/empty_widget_state/base_empty_widget.dart';

class CartScreen extends StatefulWidget {
  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  List<CartModel> _cartProducts = [];
  LanguageController? languageController;
  CartController? _cartController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
    _cartController = Get.put(
      CartController(
        cartRepository: CartRepository(
          cartService: CartService.instance,
        ),
      ),
    );
    _cartProducts = _cartController!.cart;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      isCategoryPage: false,
      isProductsPage: false,
      isProfilePage: false,
      isCartPage: true,
      onPressFunction: _cartController!.cart.isEmpty
          ? () {}
          : () => _cartController!.deleteProductsInCart(),
      pageTitle: StringsManager.cartString.tr,
      page: Obx(
        () => _cartController!.isLoading.value
            ? BaseLoading()
            : _cartProducts.isEmpty
                ? BaseEmptyWidget(language: languageController!.language)
                : Column(
                    children: [
                      Expanded(
                        child: ListView.separated(
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemBuilder: (context, index) => Dismissible(
                            key: Key(_cartProducts[index].productName),
                            background: DismissibleComponent(
                                language: languageController!.language),
                            secondaryBackground: DismissibleComponent(
                                language: languageController!.language),
                            onDismissed: (direction) =>
                                _cartController!.deleteByDismissibleDirection(
                              direction,
                              _cartProducts[index].id!,
                              languageController!.language,
                            ),
                            child: CartProductItem(
                              language: languageController!.language,
                              imageUrl: _cartProducts[index].productImage,
                              productName: _cartProducts[index].productName,
                              productQuantity: _cartProducts[index].quantity,
                              productPrice: _cartProducts[index].productPrice,
                              plusFunction: () {
                                setState(() {
                                  _cartProducts[index].quantity++;
                                });
                                _cartController!.updateProductInCart(
                                    cartModel: _cartProducts[index]);
                              },
                              minusFunction: () {
                                if (_cartProducts[index].quantity <= 1) {
                                  BaseToast.showInfo(
                                    msg: StringsManager
                                        .infoQuantityNotLessThanOneString.tr,
                                    title: StringsManager.infoString.tr,
                                    lang: languageController!.language,
                                  );
                                } else {
                                  setState(() {
                                    _cartProducts[index].quantity--;
                                  });
                                  _cartController!.updateProductInCart(
                                      cartModel: _cartProducts[index]);
                                }
                              },
                            ),
                          ),
                          separatorBuilder: (context, index) => SizedBox(
                            height:
                                getProportionateScreenHeight(SizeManager.s15),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(PaddingManager.p12),
                            vertical: getProportionateScreenHeight(PaddingManager.p15),
                          ),
                          itemCount: _cartProducts.length,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: getProportionateScreenWidth(
                            PaddingManager.p12,
                          ),
                        ),
                        child: TotalPriceAndCount(
                          language: languageController!.language,
                          totalQuantity: _cartController!.getTotalQuantity(),
                          totalPrice: _cartController!.getTotalPrice(),
                        ),
                      ),
                      const SizedBox(height: SizeManager.s50),
                    ],
                  ),
      ),
    );
  }
}
