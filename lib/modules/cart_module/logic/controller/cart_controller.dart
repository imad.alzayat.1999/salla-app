import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:shop_application/models/cart/cart_model.dart';
import 'package:shop_application/data/repository/cart_repository/cart_repository.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import '../../../../shared/resourses/consts_manager.dart';
import '../../../../shared/resourses/strings_manager.dart';
import '../../../../shared/widgets/base/base_toast.dart';

class CartController extends GetxController {
  final isLoading = false.obs;
  final CartRepository cartRepository;
  final cart = <CartModel>[].toSet().toList().obs;
  CartModel? cartModel;

  CartController({required this.cartRepository});

  @override
  void onInit() async {
    super.onInit();
    await getProductsFromCart();
  }

  updateProducts() {
    cart.refresh();
  }

  void addProductToCart({
    required int productId,
    required int quantity,
    required String productName,
    required String productDescription,
    required String productImage,
    required double productOldPrice,
    required double productPrice,
    required int productDiscount,
    required String language,
  }) {
    String? userToken = LocalStorage.getData(key: "token");
    cartModel = CartModel(
      productId: productId,
      productName: productName,
      productDescription: productDescription,
      productImage: productImage,
      productOldPrice: productOldPrice,
      productPrice: productPrice,
      productDiscount: productDiscount,
      quantity: quantity,
      userToken: userToken!
    );
    if (checkIfProductsIsExistedInTheCart(productId)) {
      BaseToast.showWarning(
        msg: StringsManager.productInCartString.tr,
        title: StringsManager.warningString.tr,
        lang: language,
      );
    } else {
      isLoading.value = true;
      cartRepository.insertProductIntoCart(cartModel: cartModel!).then((value) {
        isLoading.value = false;
        cart.add(value);
        BaseToast.showDone(
          msg: StringsManager.successAddToCart.tr,
          title: StringsManager.successString.tr,
          lang: language,
        );
      }).catchError((error) {
        print(error.toString());
        isLoading.value = false;
      });
    }
  }

  getProductsFromCart() async {
    isLoading(true);
    await cartRepository.getAllProductsFromCart().then((value) {
      isLoading(false);
      cart.addAll(value);
      updateProducts();
    }).catchError((error) {
      isLoading(false);
      print(error.toString());
    });
    isLoading(false);
    update();
  }

  updateProductInCart({required CartModel cartModel}) async {
    await cartRepository
        .updateProductInCart(cartModel: cartModel)
        .then((value) {
      cart[cart.indexWhere((element) => element.id == cartModel.id)] =
          cartModel;
      updateProducts();
    }).catchError((error) {
      print(error.toString());
    });
    isLoading(false);
    update();
  }

  deleteProductInCart({required int id, required String language}) async {
    isLoading(false);
    await cartRepository.deleteProductFromCart(id: id).then((value) {
      isLoading(false);
      cart.removeWhere((element) => element.id == id);
      updateProducts();
      BaseToast.showDone(
        msg: StringsManager.successDeleteProductFromCart.tr,
        title: StringsManager.successString.tr,
        lang: language,
      );
    }).catchError((error) {
      isLoading(false);
      print(error.toString());
    });
    isLoading(false);
    update();
  }

  deleteProductsInCart() async {
    isLoading(false);
    await cartRepository.deleteCartTable().then((value) {
      isLoading(false);
      cart.clear();
      updateProducts();
    }).catchError((error) {
      isLoading(false);
      print(error.toString());
    });
    isLoading(false);
    update();
  }

  bool checkIfProductsIsExistedInTheCart(int id) {
    return cart.any((p0) => p0.productId == id);
  }

  deleteByDismissibleDirection(
      DismissDirection direction, int productId, String language) {
    if (language == ConstsManager.en) {
      if (direction == DismissDirection.endToStart) {
        deleteProductInCart(id: productId, language: language);
      }
    } else {
      if (direction == DismissDirection.startToEnd) {
        deleteProductInCart(id: productId, language: language);
      }
    }
  }

  double getTotalPrice() {
    double totalPrice = 0.0;
    cart.forEach((element) {
      totalPrice += element.productPrice * element.quantity;
    });
    return totalPrice;
  }

  int getTotalQuantity() {
    int totalQuantity = 0;
    cart.forEach((element) {
      totalQuantity += element.quantity;
    });
    return totalQuantity;
  }
}
