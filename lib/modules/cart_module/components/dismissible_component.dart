import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';
import 'package:get/get.dart';

class DismissibleComponent extends StatelessWidget {
  final String language;

  const DismissibleComponent({Key? key, required this.language})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorsManager.kDismissibleColor,
      child: Padding(
        padding: const EdgeInsets.all(PaddingManager.p12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            const BaseIcon(
              iconHeight: SizeManager.s20,
              iconPath: IconManager.trashIcon,
              iconColor: ColorsManager.kBlackColor,
            ),
            Text(
              StringsManager.moveToTrashString.tr,
              style: language == ConstsManager.en
                  ? getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
