import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class CartProductItem extends StatefulWidget {
  final String imageUrl;
  final String productName;
  int productQuantity;
  final double productPrice;
  final void Function()? minusFunction;
  final void Function()? plusFunction;
  final String language;

  CartProductItem(
      {required this.imageUrl,
      required this.productName,
      required this.productQuantity,
      required this.productPrice,
      required this.plusFunction,
      required this.minusFunction,
      required this.language});

  @override
  State<CartProductItem> createState() => _cartProductItemState();
}

class _cartProductItemState extends State<CartProductItem> {
  @override
  Widget build(BuildContext context) {
    String totalPrice =
        (widget.productQuantity * widget.productPrice).toString();
    return SizedBox(
      child: Row(
        children: [
          SizedBox(
            width: getProportionateScreenHeight(SizeManager.s100),
            height: getProportionateScreenHeight(SizeManager.s100),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(SizeManager.s12),
              child: BaseNetworkImage(
                imageColor: ColorsManager.transparentColor,
                imageHeight: getProportionateScreenHeight(SizeManager.s100),
                imageUrl: widget.imageUrl,
                imageBoxFit: BoxFit.contain,
              ),
            ),
          ),
          SizedBox(width: getProportionateScreenWidth(SizeManager.s10)),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.productName,
                  style: widget.language == ConstsManager.en
                      ? getRegularTextStyleBaloo2(
                          textColor: ColorsManager.kBlackColor,
                          textOverflow: TextOverflow.ellipsis,
                        )
                      : getRegularTextStyleTajawal(
                          textColor: ColorsManager.kBlackColor,
                          textOverflow: TextOverflow.ellipsis),
                  maxLines: SizeManager.s2.toInt(),
                ),
                Row(
                  children: [
                    const Center(
                      child: BaseIcon(
                        iconColor: ColorsManager.kBlackColor,
                        iconHeight: SizeManager.s12,
                        iconPath: IconManager.dollarIcon,
                      ),
                    ),
                    SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
                    Text(
                      widget.productPrice.toString(),
                      style: getRegularTextStyleBaloo2(
                        textColor: ColorsManager.kBlackColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(SizeManager.s25)),
                Row(
                  children: [
                    Text(
                      StringsManager.subtotalString.tr,
                      style: widget.language == ConstsManager.en
                          ? getBoldTextStyleBaloo2(
                              textColor: ColorsManager.kBlackColor,
                              fontSize: FontSizeManager.size16,
                            )
                          : getBoldTextStyleTajawal(
                              textColor: ColorsManager.kBlackColor,
                              fontSize: FontSizeManager.size16,
                            ),
                    ),
                    Row(
                      children: [
                        const Center(
                          child: BaseIcon(
                            iconColor: ColorsManager.kBlackColor,
                            iconHeight: SizeManager.s14,
                            iconPath: IconManager.dollarIcon,
                          ),
                        ),
                        SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
                        Text(
                          totalPrice,
                          style: getRegularTextStyleBaloo2(
                            textColor: ColorsManager.kBlackColor,
                            fontSize: FontSizeManager.size14
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Row(
            children: [
              GestureDetector(
                child: const BaseIcon(
                  iconPath: IconManager.minusIcon,
                  iconHeight: SizeManager.s20,
                  iconColor: ColorsManager.kBlackColor,
                ),
                onTap: widget.minusFunction,
              ),
              SizedBox(width: getProportionateScreenWidth(SizeManager.s10)),
              Text(widget.productQuantity.toString()),
              SizedBox(width: getProportionateScreenWidth(SizeManager.s10)),
              GestureDetector(
                onTap: widget.plusFunction,
                child: const BaseIcon(
                  iconPath: IconManager.plusIcon,
                  iconHeight: SizeManager.s20,
                  iconColor: ColorsManager.kBlackColor,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
