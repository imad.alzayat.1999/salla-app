import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

class TotalPriceAndCount extends StatelessWidget {
  final String language;
  final int totalQuantity;
  final double totalPrice;

  const TotalPriceAndCount(
      {Key? key,
      required this.language,
      required this.totalQuantity,
      required this.totalPrice})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: ColorsManager.kTotalCardColor,
      elevation: SizeManager.s3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeManager.s10),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: getProportionateScreenHeight(PaddingManager.p10),
          horizontal: getProportionateScreenWidth(PaddingManager.p12),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  StringsManager.totalQuantityString.tr,
                  style: language == ConstsManager.en
                      ? getBoldTextStyleBaloo2(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16)
                      : getBoldTextStyleTajawal(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16),
                ),
                Text(
                  totalQuantity.toString(),
                  style: language == ConstsManager.en
                      ? getBoldTextStyleBaloo2(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16)
                      : getBoldTextStyleTajawal(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16),
                ),
              ],
            ),
            SizedBox(height: getProportionateScreenHeight(SizeManager.s20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  StringsManager.totalPriceString.tr,
                  style: language == ConstsManager.en
                      ? getBoldTextStyleBaloo2(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16)
                      : getBoldTextStyleTajawal(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16),
                ),
                Text(
                  totalPrice.toString() + " \$",
                  style: language == ConstsManager.en
                      ? getBoldTextStyleBaloo2(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16)
                      : getBoldTextStyleTajawal(
                          textColor: ColorsManager.kBlackColor,
                          fontSize: FontSizeManager.size16),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
