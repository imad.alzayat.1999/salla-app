import 'package:flutter/material.dart';
import 'package:shop_application/models/authentication/login_model.dart';

abstract class LoginStates{}


class LoginInitialState extends LoginStates{}

class LoginLoadingState extends LoginStates{}

class LoginSuccessState extends LoginStates{
  final LoginModel loginModel;

  LoginSuccessState({required this.loginModel});
}

class LoginFailedState extends LoginStates{
  final String error;

  LoginFailedState(this.error);

}
