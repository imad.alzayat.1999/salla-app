import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/authentication_repository/login_repository.dart';
import 'package:shop_application/models/authentication/login_model.dart';
import 'package:shop_application/modules/login_module/logic/state/login_states.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class LoginBloc extends Cubit<LoginStates> {
  LoginBloc({required this.loginRepository}) : super(LoginInitialState());

  static LoginBloc get(context) => BlocProvider.of(context);
  LoginModel? loginModel;
  LoginRepository loginRepository;

  void loginUser({
    required String email,
    required String password,
    required String language,
  }) {
    emit(LoginLoadingState());
    loginRepository
        .loginAccountRepository(
            email: email, password: password, language: language)
        .then((value) {
      loginModel = LoginModel.fromJson(value);
      emit(
        LoginSuccessState(loginModel: loginModel!),
      );
    }).catchError((error) {
      print(error.toString());
      emit(LoginFailedState(error.toString()));
    });
  }
}
