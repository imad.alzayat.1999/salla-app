import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/authentication_repository/login_repository.dart';
import 'package:shop_application/data/service/authentication_service/login_service.dart';
import 'package:shop_application/modules/login_module/components/signup_link.dart';
import 'package:shop_application/modules/login_module/logic/bloc/login_bloc.dart';
import 'package:shop_application/modules/login_module/logic/state/login_states.dart';
import 'package:shop_application/modules/products_module/views/products_screen.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/validation_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/base/base_background_image.dart';
import 'package:shop_application/shared/widgets/base/base_button.dart';
import 'package:shop_application/shared/widgets/base/base_text_field.dart';
import 'package:shop_application/shared/widgets/base/base_toast.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/resourses/fonts_manager.dart';
import '../../../shared/resourses/styles_manager/styles_baloo2_manager.dart';
import '../../../shared/resourses/styles_manager/styles_tajawal_manager.dart';
import '../../../shared/config/size_config.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var emailController = TextEditingController();
  var passController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BlocProvider(
      create: (context) => LoginBloc(
        loginRepository: LoginRepository(
          loginService: LoginService(),
        ),
      ),
      child: BlocConsumer<LoginBloc, LoginStates>(listener: (context, state) {
        if (state is LoginSuccessState) {
          if (state.loginModel.status) {
            LocalStorage.putData(key: "password", value: passController.text);
            LocalStorage.putData(
                key: "token", value: state.loginModel.data!.token);
            Get.to(ProductScreen());
          } else {
            BaseToast.showError(
              msg: state.loginModel.message,
              title: StringsManager.errorString.tr,
              lang: languageController!.language,
            );
          }
        }
      }, builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: Stack(
              children: [
                BaseBackgroundImage(),
                Center(
                  child: Form(
                    key: formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(PaddingManager.p12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              StringsManager.emailString.tr,
                              style: languageController!.language ==
                                      ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                           SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            BaseTextField(
                              language: languageController!.language,
                              textInputType: TextInputType.emailAddress,
                              obsText: false,
                              controller: emailController,
                              iconData: IconManager.emailIcon,
                              onSubmit: (String) {},
                              onValidate: (String? email) {
                                if (!ValidationManager.isEmailValid(email!)) {
                                  return StringsManager
                                      .validationEmailString.tr;
                                }
                                return null;
                              },
                            ),
                           SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                            Text(
                              StringsManager.passwordString.tr,
                              style: languageController!.language ==
                                      ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                           SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            BaseTextField(
                              language: languageController!.language,
                              textInputType: TextInputType.visiblePassword,
                              obsText: true,
                              controller: passController,
                              isPassword: true,
                              iconData: IconManager.passwordIcon,
                              onSubmit: (String) {},
                              onValidate: (String? password) {
                                if (!ValidationManager.isPasswordValid(
                                    password!)) {
                                  return StringsManager.validationPassString.tr;
                                }
                                return null;
                              },
                            ),
                           SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                            state is LoginLoadingState
                                ? BaseLoading()
                                : BaseButton(
                                    btnTextColor: ColorsManager.kBlackColor,
                                    language: languageController!.language,
                                    btnText: StringsManager.loginString.tr,
                                    onPress: () {
                                      if (formKey.currentState!.validate()) {
                                        LoginBloc.get(context).loginUser(
                                          email: emailController.text,
                                          password: passController.text,
                                          language:
                                              languageController!.language,
                                        );
                                      }
                                    },
                                    btnColor:
                                        ColorsManager.kAuthenticationBtnColor,
                                  ),
                            const SizedBox(height: SizeManager.s20),
                            SignupLink(language: languageController!.language),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
