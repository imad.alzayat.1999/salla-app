import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/signup_module/view/signup_screen.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';

class SignupLink extends StatelessWidget {
  final String language;

  const SignupLink({Key? key, required this.language}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          StringsManager.registerQuestionString.tr,
          style: language == ConstsManager.en
              ? getRegularTextStyleBaloo2(textColor: ColorsManager.kBlackColor)
              : getRegularTextStyleTajawal(textColor: ColorsManager.kBlackColor),
        ),
        TextButton(
          onPressed: () {
            Get.to(SignupScreen());
          },
          child: Text(
            StringsManager.registerString.tr,
            style: language == ConstsManager.en
                ? getRegularTextStyleBaloo2(textColor: ColorsManager.kBlackColor)
                : getRegularTextStyleTajawal(
                    textColor: ColorsManager.kBlackColor),
          ),
        ),
      ],
    );
  }
}
