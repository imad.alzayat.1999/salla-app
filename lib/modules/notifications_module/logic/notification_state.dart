import 'package:shop_application/models/notification/notification_model.dart';

abstract class NotificationState{}

class NotificationInitialState extends NotificationState{}

class NotificationLoadingState extends NotificationState{}

class NotificationSuccessState extends NotificationState{
  final List<NotificationItem> notifications;

  NotificationSuccessState(this.notifications);
}

class NotificationFailedState extends NotificationState{
  final String error;

  NotificationFailedState(this.error);

}