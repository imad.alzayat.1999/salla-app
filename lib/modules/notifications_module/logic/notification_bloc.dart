import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../data/repository/notification_repository/notification_repository.dart';
import '../../../models/notification/notification_model.dart';
import 'notification_state.dart';

class NotificationBloc extends Cubit<NotificationState>{
  NotificationBloc(this.notificationRepository) : super(NotificationInitialState());

  final NotificationRepository notificationRepository;
  List<NotificationItem> notifications = [];

  fetchNotifications({required String language}){
    emit(NotificationLoadingState());
    notificationRepository.getNotificationsRepository(language: language).then((value){
      notifications.addAll(value);
      emit(NotificationSuccessState(notifications));
    }).catchError((error){
      print(error.toString());
      emit(NotificationFailedState(error.toString()));
    });
  }
}