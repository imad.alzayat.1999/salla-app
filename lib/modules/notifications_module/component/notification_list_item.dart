import 'package:flutter/material.dart';
import 'package:shop_application/models/notification/notification_model.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';
import 'dart:math' as math;

class NotificationListItem extends StatelessWidget {
  final NotificationItem notification;
  final String language;

  const NotificationListItem({
    required this.notification,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Transform.rotate(
        angle:
            language == ConstsManager.en ? math.pi / 180 : math.pi,
        child: BaseIcon(
          iconHeight: SizeManager.s20,
          iconPath: IconManager.pointIcon,
          iconColor: ColorsManager.kIconColor,
        ),
      ),
      subtitle: Text(
        notification.message,
        style: language == ConstsManager.en
            ? getRegularTextStyleBaloo2(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              )
            : getRegularTextStyleTajawal(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              ),
      ),
      title: Text(
        notification.title,
        style: language == ConstsManager.en
            ? getSemiBoldTextStyleBaloo2(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size24,
              )
            : getSemiBoldTextStyleTajawal(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size24,
              ),
      ),
    );
  }
}
