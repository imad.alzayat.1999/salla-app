import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/notification_repository/notification_repository.dart';
import 'package:shop_application/data/service/notification_service/notification_service.dart';
import 'package:shop_application/models/notification/notification_model.dart';
import 'package:shop_application/modules/notifications_module/component/notification_list_item.dart';
import 'package:shop_application/modules/notifications_module/logic/notification_bloc.dart';
import 'package:shop_application/modules/notifications_module/logic/notification_state.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/widget_states/empty_widget_state/base_empty_widget.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../../shared/base_application_page/base_application_page.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';

class NotificationScreen extends StatefulWidget {
  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      isCategoryPage: false,
      isProductsPage: false,
      isProfilePage: false,
      pageTitle: StringsManager.notificationsString.tr,
      page: BlocProvider(
        create: (context) => NotificationBloc(
          NotificationRepository(
            NotificationService(),
          ),
        )..fetchNotifications(language: languageController!.language),
        child: BlocConsumer<NotificationBloc, NotificationState>(
          listener: (context, state) {},
          builder: (context, state) {
            List<NotificationItem> notifications = [];
            if (state is NotificationLoadingState) {
              return BaseLoading();
            }
            if (state is NotificationSuccessState) {
              notifications = state.notifications;
            }
            return notifications.isEmpty
                ? BaseEmptyWidget(language: languageController!.language)
                : SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        ListView.separated(
                          physics: const BouncingScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) => NotificationListItem(
                            notification: notifications[index],
                            language: languageController!.language,
                          ),
                          separatorBuilder: (context, index) => SizedBox(
                            height: getProportionateScreenHeight(SizeManager.s10),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(PaddingManager.p12),
                            vertical: getProportionateScreenHeight(PaddingManager.p15),
                          ),
                          itemCount: notifications.length,
                        ),
                        const SizedBox(height: SizeManager.s50),
                      ],
                    ),
                  );
          },
        ),
      ),
    );
  }
}
