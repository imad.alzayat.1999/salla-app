import 'package:shop_application/models/authentication/login_model.dart';
import 'package:shop_application/models/profile/profile_model.dart';

abstract class ProfileStates {}

class ProfileInitialState extends ProfileStates {}

class ProfileLoadingState extends ProfileStates {}

class ProfileSuccessState extends ProfileStates {
  final Profile profile;

  ProfileSuccessState({required this.profile});
}

class ProfileFailedState extends ProfileStates {
  final String error;

  ProfileFailedState(this.error);
}

class UpdateProfileDataSuccessState extends ProfileStates {
  final Profile profile;

  UpdateProfileDataSuccessState(this.profile);
}

class UpdateProfileDataFailedState extends ProfileStates {
  final String error;

  UpdateProfileDataFailedState(this.error);
}

class FilePickedSuccessState extends ProfileStates {}

class FilePickedLoadingState extends ProfileStates {}
