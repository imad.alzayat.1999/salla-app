import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/profile_repository/profile_repository.dart';
import 'package:shop_application/models/profile/profile_model.dart';
import 'package:shop_application/modules/profile_module/logic/states/profile_states.dart';

class ProfileBloc extends Cubit<ProfileStates> {
  ProfileBloc({required this.profileRepository}) : super(ProfileInitialState());

  static ProfileBloc get(context) => BlocProvider.of<ProfileBloc>(context);
  ProfileRepository profileRepository;
  Profile? profile;
  File? imageFile;

  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();

  void fetchUserInformation({required String language}) {
    emit(ProfileLoadingState());
    profileRepository.getProfileInformation(language: language).then((value) {
      profile = value;
      nameController.text = profile!.name;
      emailController.text = profile!.email;
      phoneController.text = profile!.phone;
      emit(ProfileSuccessState(profile: profile!));
    }).catchError((error) {
      print(error.toString());
      emit(ProfileFailedState(error.toString()));
    });
  }

  void updateProfileInformation({
    required String language,
    required String image,
    required String name,
    required String email,
    required String phone,
    required String password,
  }) {
    emit(ProfileLoadingState());
    profileRepository
        .updateProfileInformation(
      language: language,
      email: email,
      image: image,
      name: name,
      password: password,
      phone: phone,
    )
        .then((value) {
      profile = value;
      emit(UpdateProfileDataSuccessState(profile!));
      fetchUserInformation(language: language);
    }).catchError((error) {
      print(error.toString());
      emit(UpdateProfileDataFailedState(error.toString()));
    });
  }
}
