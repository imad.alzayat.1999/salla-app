import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/profile_repository/profile_repository.dart';
import 'package:shop_application/data/service/profile_service/profile_service.dart';
import 'package:shop_application/modules/profile_module/components/profile_page.dart';
import 'package:shop_application/modules/profile_module/logic/bloc/profile_bloc.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import '../../../shared/config/size_config.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  var emailController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      isProfilePage: false,
      isProductsPage: false,
      isCategoryPage: false,
      pageTitle: StringsManager.profileEditString.tr,
      page: BlocProvider(
        create: (context) => ProfileBloc(
          profileRepository: ProfileRepository(
            profileService: ProfileService(),
          ),
        ),
        child: ProfilePage(language: languageController!.language),
      ),
    );
  }
}
