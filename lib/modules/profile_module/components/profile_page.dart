import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/profile_info_screen/view/profile_info_screen.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_toast.dart';
import '../../../models/profile/profile_model.dart';
import '../../../shared/resourses/validation_manager.dart';
import '../../../shared/widgets/base/base_button.dart';
import '../../../shared/widgets/base/base_text_field.dart';
import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../logic/bloc/profile_bloc.dart';
import '../logic/states/profile_states.dart';

class ProfilePage extends StatefulWidget {
  final String language;

  const ProfilePage({Key? key, required this.language}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var nameController = TextEditingController();
  String? password = LocalStorage.getData(key: "password");
  var globalKey = GlobalKey<FormState>();

  Profile? profile;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context)
        .fetchUserInformation(language: widget.language);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileBloc, ProfileStates>(
      listener: (context, states) {
        if (states is UpdateProfileDataSuccessState) {
          BaseToast.showDone(
            msg: StringsManager.successEditProfileString.tr,
            title: StringsManager.successString.tr,
            lang: widget.language,
          );
          Get.off(ProfileInfoScreen());
        }
      },
      builder: (context, states) {
        if (states is ProfileLoadingState) {
          return BaseLoading();
        }
        if (states is ProfileSuccessState) {
          profile = states.profile;
          nameController.text = profile!.name;
          print(states.profile.image);
        }
        return Padding(
          padding: const EdgeInsets.all(PaddingManager.p12),
          child: Center(
            child: Form(
              key: globalKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BaseTextField(
                    language: widget.language,
                    textInputType: TextInputType.name,
                    obsText: false,
                    controller: ProfileBloc.get(context).nameController,
                    iconData: IconManager.userIcon,
                    onSubmit: (String) {},
                    onValidate: (String? name) {
                      if (!ValidationManager.isTextValid(name!)) {
                        return StringsManager.validationNameString.tr;
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                  BaseTextField(
                    language: widget.language,
                    textInputType: TextInputType.emailAddress,
                    obsText: false,
                    controller: ProfileBloc.get(context).emailController,
                    iconData: IconManager.emailIcon,
                    onSubmit: (String) {},
                    onValidate: (String? email) {
                      if (!ValidationManager.isEmailValid(email!)) {
                        return StringsManager.validationEmailString.tr;
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                  BaseTextField(
                    language: widget.language,
                    textInputType: TextInputType.phone,
                    obsText: false,
                    controller: ProfileBloc.get(context).phoneController,
                    iconData: IconManager.phoneIcon,
                    onSubmit: (String) {},
                    onValidate: (String? phone) {
                      if (!ValidationManager.isPhoneValid(phone!)) {
                        return StringsManager.validationPhoneString.tr;
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                  BaseButton(
                    language: widget.language,
                    btnTextColor: ColorsManager.kBlackColor,
                    btnText: StringsManager.updateString.tr,
                    onPress: () {
                      print(ProfileBloc.get(context).nameController.text);
                      if (globalKey.currentState!.validate()) {
                        globalKey.currentState!.save();
                        ProfileBloc.get(context).updateProfileInformation(
                          language: widget.language,
                          image: profile!.image,
                          name: ProfileBloc.get(context).nameController.text,
                          email: ProfileBloc.get(context).emailController.text,
                          phone: ProfileBloc.get(context).phoneController.text,
                          password: password ?? "",
                        );
                      }
                    },
                    btnColor: ColorsManager.kUpdateProfileBtnColor,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
