import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/products_module/views/products_screen.dart';
import 'package:shop_application/modules/select_language_module/language_screen.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String? token = LocalStorage.getData(key: "token");

  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(seconds: ConstsManager.durationInSecondsSplashScreen),
      routeToAnotherPage,
    );
  }

  routeToAnotherPage() {
    Get.offAll(token == null ? const SelectLanguageScreen() : ProductScreen());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: ColorsManager.kWhiteColor,
      body: Container(
        height: getProportionateScreenHeight(double.infinity),
        width: getProportionateScreenWidth(double.infinity),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ImageManager.shoppingLogo),
          ),
        ),
      ),
    );
  }
}
