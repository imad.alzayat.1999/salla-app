import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/resourses/consts_manager.dart';
import '../../../shared/resourses/fonts_manager.dart';
import '../../../shared/resourses/styles_manager/styles_baloo2_manager.dart';
import '../../../shared/resourses/styles_manager/styles_tajawal_manager.dart';

class FaqExpansionTile extends StatelessWidget {
  final String question;
  final String answer;
  final String language;

  const FaqExpansionTile({
    Key? key,
    required this.question,
    required this.answer,
    required this.language,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(
        question,
        style: language == ConstsManager.en
            ? getBoldTextStyleBaloo2(
                textColor: ColorsManager.dividerColor,
                fontSize: FontSizeManager.size20,
              )
            : getBoldTextStyleTajawal(
                textColor: ColorsManager.dividerColor,
                fontSize: FontSizeManager.size20,
              ),
      ),
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(
              PaddingManager.p18,
            ),
          ),
          child: Align(
            alignment: language == ConstsManager.en
                ? Alignment.centerLeft
                : Alignment.centerRight,
            child: Text(
              answer,
              style: language == ConstsManager.en
                  ? getRegularTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getRegularTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
          ),
        ),
      ],
    );
  }
}
