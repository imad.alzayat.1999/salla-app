import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/modules/faq_module/component/faq_section.dart';
import 'package:shop_application/modules/faq_module/logic/faq_bloc.dart';
import 'package:shop_application/modules/faq_module/logic/faq_states.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';

import '../../../models/faq/faq_model.dart';
import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';

class FaqPage extends StatefulWidget {
  final String language;

  const FaqPage({Key? key, required this.language}) : super(key: key);

  @override
  _FaqPageState createState() => _FaqPageState();
}

class _FaqPageState extends State<FaqPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FaqBloc>(context).getFAQs(language: widget.language);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FaqBloc, FaqStates>(
      listener: (context, state) {},
      builder: (context, state) {
        List<FAQ> faqs = [];
        if (state is FaqLoadingState) {
          return BaseLoading();
        }
        if (state is FaqSuccessState) {
          faqs = state.faqs;
        }
        return SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(PaddingManager.p12),
            child: FaqSection(
              listOfQuestions: faqs,
              language: widget.language,
            ),
          ),
        );
      },
    );
  }
}
