import 'package:flutter/material.dart';
import 'package:shop_application/models/faq/faq_model.dart';
import 'package:shop_application/modules/faq_module/component/faq_expansion_tile.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

class FaqSection extends StatelessWidget {
  final List<FAQ> listOfQuestions;
  final String language;

  const FaqSection({
    required this.listOfQuestions,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListView.separated(
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return FaqExpansionTile(
                question: listOfQuestions[index].question,
                answer: listOfQuestions[index].answer,
                language: language);
          },
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(PaddingManager.p12),
            vertical: getProportionateScreenHeight(PaddingManager.p15),
          ),
          separatorBuilder: (context, index) => SizedBox(
            height: getProportionateScreenHeight(SizeManager.s20),
          ),
          itemCount: listOfQuestions.length,
        ),
        const SizedBox(height: SizeManager.s50),
      ],
    );
  }
}
