import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/faq_repository/faq_repository.dart';
import 'package:shop_application/models/faq/faq_model.dart';

import '../../faq_module/logic/faq_states.dart';


class FaqBloc extends Cubit<FaqStates>{
  FaqBloc({required this.faqRepository}) : super(FaqInitialState());
  FAQRepository faqRepository;
  List<FAQ> faqs = [];

  void getFAQs({required String language}){
    emit(FaqLoadingState());
    faqRepository.getFaqRepository(language: language).then((value){
      faqs.addAll(value);
      emit(FaqSuccessState(faqs));
    }).catchError((error){
      emit(FaqFailedState(error.toString()));
    });
  }
}