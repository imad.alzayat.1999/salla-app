import 'package:shop_application/models/faq/faq_model.dart';

abstract class FaqStates{}

class FaqInitialState extends FaqStates{}

class FaqLoadingState extends FaqStates{}

class FaqSuccessState extends FaqStates{
  final List<FAQ> faqs;

  FaqSuccessState(this.faqs);

}

class FaqFailedState extends FaqStates{
  final String error;

  FaqFailedState(this.error);

}