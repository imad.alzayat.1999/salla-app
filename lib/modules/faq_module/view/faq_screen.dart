import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/faq_repository/faq_repository.dart';
import 'package:shop_application/data/service/faq_service/faq_service.dart';
import 'package:shop_application/modules/faq_module/component/faq_page.dart';
import 'package:shop_application/modules/faq_module/logic/faq_bloc.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';

class FaqScreen extends StatefulWidget {
  const FaqScreen({Key? key}) : super(key: key);

  @override
  State<FaqScreen> createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GetBuilder<LanguageController>(
      init: LanguageController(),
      builder: (controller) {
        return BaseApplicationPage(
          onPressFunction: (){},
          pageTitle: StringsManager.faqString.tr,
          page: BlocProvider(
            create: (context) => FaqBloc(
              faqRepository: FAQRepository(
                faqService: FAQService(),
              ),
            ),
            child: FaqPage(
              language: languageController!.language,
            ),
          ),
          isProductsPage: false,
          isCategoryPage: false,
          isProfilePage: false,
        );
      },
    );
  }
}
