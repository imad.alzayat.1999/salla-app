import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/contact_repository/contact_repository.dart';
import 'package:shop_application/data/service/contact_service/contact_service.dart';
import 'package:shop_application/modules/contact_info_module/components/contact_page.dart';
import 'package:shop_application/modules/contact_info_module/logic/contact_bloc.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import '../../../shared/base_application_page/base_application_page.dart';
import '../../../shared/config/size_config.dart';

class ContactInfoScreen extends StatefulWidget {
  @override
  State<ContactInfoScreen> createState() => _ContactInfoScreenState();
}

class _ContactInfoScreenState extends State<ContactInfoScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      pageTitle: StringsManager.contactsString.tr,
      isCategoryPage: false,
      isProductsPage: false,
      isProfilePage: false,
      page: BlocProvider(
        create: (context) => ContactBloc(
          contactRepository: ContactRepository(
            contactService: ContactService(),
          ),
        ),
        child: ContactPage(language: languageController!.language),
      ),
    );
  }
}
