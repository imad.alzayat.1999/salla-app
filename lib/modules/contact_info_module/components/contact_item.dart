import 'package:flutter/material.dart';
import 'package:shop_application/models/contact/contact_model.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class ContactItem extends StatelessWidget {
  final Contact contact;
  final void Function()? onPressFunction;
  final String contactIcon;
  final Color contactIconColor;
  final String contactName;
  final String language;

  const ContactItem({
    Key? key,
    required this.contact,
    required this.onPressFunction,
    required this.contactIcon,
    required this.contactIconColor,
    required this.contactName,
    required this.language,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(contactIcon);
    return ListTile(
      leading: BaseIcon(
        iconColor: contactIconColor,
        iconHeight: SizeManager.s43,
        iconPath: contactIcon,
      ),
      title: Text(
        contactName,
        style: language == ConstsManager.en
            ? getRegularTextStyleBaloo2(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              )
            : getRegularTextStyleTajawal(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              ),
      ),
      trailing: IconButton(
        onPressed: onPressFunction,
        icon: Icon(
          language == ConstsManager.en
              ? Icons.keyboard_arrow_right
              : Icons.keyboard_arrow_left,
          size: 40,
          color: ColorsManager.kIconColor,
        ),
      ),
    );
  }
}
