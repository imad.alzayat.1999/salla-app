import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/models/contact/contact_model.dart';
import 'package:shop_application/modules/contact_info_module/components/contact_item.dart';
import 'package:shop_application/modules/contact_info_module/logic/contact_bloc.dart';
import 'package:shop_application/modules/contact_info_module/logic/contact_states.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';

class ContactPage extends StatefulWidget {
  final String language;

   ContactPage({Key? key, required this.language}) : super(key: key);

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ContactBloc>(context)
        .fetchContacts(language: widget.language);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ContactBloc, ContactStates>(
      listener: (context, state) {},
      builder: (context, state) {
        List<Contact> contacts = [];
        if (state is ContactLoadingState) {
          return BaseLoading();
        }
        if (state is ContactSuccessState) {
          contacts = state.contacts;
        }
        return SingleChildScrollView(
          physics:  BouncingScrollPhysics(),
          child: Padding(
            padding:  EdgeInsets.all(PaddingManager.p12),
            child: Column(
              children: [
                ListView.separated(
                  physics:  BouncingScrollPhysics(),
                  shrinkWrap: true,
                  padding:  EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(PaddingManager.p12),
                    vertical: getProportionateScreenHeight(PaddingManager.p15),
                  ),
                  itemBuilder: (context, index) => ContactItem(
                    contactName: ContactBloc.get(context).getContactName(index),
                    language: widget.language,
                    contactIconColor:
                        ContactBloc.get(context).getContactIconColor(index),
                    contactIcon: ContactBloc.get(context).getContactIcon(index),
                    contact: contacts[index],
                    onPressFunction: () => ContactBloc.get(context).launchURL(
                      contacts[index].value,
                    ),
                  ),
                  separatorBuilder: (context, index) =>
                       SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
                  itemCount: contacts.length,
                ),
                 SizedBox(height: SizeManager.s50),
              ],
            ),
          ),
        );
      },
    );
  }
}
