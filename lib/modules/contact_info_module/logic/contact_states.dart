import 'package:shop_application/models/contact/contact_model.dart';

abstract class ContactStates{}

class ContactInitState extends ContactStates{}

class ContactLoadingState extends ContactStates{}

class ContactSuccessState extends ContactStates{
  final List<Contact> contacts;

  ContactSuccessState({required this.contacts});
}

class ContactFailedState extends ContactStates{}