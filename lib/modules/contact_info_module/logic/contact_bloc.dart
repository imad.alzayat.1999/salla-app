import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/contact_repository/contact_repository.dart';
import 'package:shop_application/modules/contact_info_module/logic/contact_states.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../models/contact/contact_model.dart';

class ContactBloc extends Cubit<ContactStates>{
  ContactBloc({required this.contactRepository}) : super(ContactInitState());

  static ContactBloc get(context) => BlocProvider.of<ContactBloc>(context);

  final ContactRepository contactRepository;
  List<Contact> contacts = [];


  fetchContacts({required String language}){
    emit(ContactLoadingState());
    contactRepository.getContactRepository(language: language).then((value){
      contacts.addAll(value);
      emit(ContactSuccessState(contacts: contacts));
    }).catchError((error){
      print(error.toString());
      emit(ContactFailedState());
    });
  }

  void launchURL(String link) async {
    if (!await launch(link)) throw 'Could not launch $link';
  }


  String getContactIcon(int index){
    switch(index){
      case 0:
        return IconManager.facebookIcon;
      case 1:
        return IconManager.instagramIcon;
      case 2:
        return IconManager.twitterIcon;
      case 3:
        return IconManager.emailIcon;
      case 4:
        return IconManager.phoneIcon;
      case 5:
        return IconManager.whatsappIcon;
      case 6:
        return IconManager.snapchatIcon;
      case 7:
        return IconManager.youtubeIcon;
      case 8:
        return IconManager.worldWideWebIcon;
      default:
        return "";
    }
  }

  String getContactName(int index){
    switch(index){
      case 0:
        return "facebook".tr;
      case 1:
        return "instagram".tr;
      case 2:
        return "twitter".tr;
      case 3:
        return "email".tr;
      case 4:
        return "phone".tr;
      case 5:
        return "whatsapp".tr;
      case 6:
        return "snapchat".tr;
      case 7:
        return "youtube".tr;
      case 8:
        return "website".tr;
      default:
        return "";
    }
  }

  Color getContactIconColor(int index){
    switch(index){
      case 0:
        return ColorsManager.facebookColor;
      case 1:
        return ColorsManager.instagramColor;
      case 2:
        return ColorsManager.twitterColor;
      case 3:
        return ColorsManager.kBlackColor;
      case 4:
        return ColorsManager.kBlackColor;
      case 5:
        return ColorsManager.whatsappColor;
      case 6:
        return ColorsManager.snapchatColor;
      case 7:
        return ColorsManager.youtubeColor;
      case 8:
        return ColorsManager.websiteColor;
      default:
        return ColorsManager.kWhiteColor;
    }
  }

}