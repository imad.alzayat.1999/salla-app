import 'package:get/get.dart';
import 'package:shop_application/data/repository/favorite_repository/favorite_repository.dart';
import 'package:shop_application/models/favorites/favorites_model.dart';
import 'package:shop_application/shared/widgets/base/base_toast.dart';

import '../../../../shared/local_storage/local_storage.dart';
import '../../../../shared/resourses/strings_manager.dart';

class FavoritesController extends GetxController{
  final isLoading = false.obs;
  final FavoriteRepository favoriteRepository;
  final favorites = <FavoritesModel>[].toSet().toList().obs;
  FavoritesModel? favoritesModel;

  FavoritesController({required this.favoriteRepository});

  @override
  void onInit() async{
    super.onInit();
    await getAllProductsInFavorites();
  }

  updateProductsFavorites() {
    favorites.refresh();
  }

  void addProductToFavorites({
    required int productId,
    required String productName,
    required String productDescription,
    required String productImage,
    required double productOldPrice,
    required double productPrice,
    required int productDiscount,
    required String language,
  }) {
    String? userToken = LocalStorage.getData(key: "token");
    favoritesModel = FavoritesModel(
      productId: productId,
      productName: productName,
      productDescription: productDescription,
      productImage: productImage,
      productOldPrice: productOldPrice,
      productPrice: productPrice,
      productDiscount: productDiscount,
      userToken: userToken!,
    );
    if (checkIfProductsIsExistedInFavorites(productId)) {
      BaseToast.showWarning(
        msg: StringsManager.productInCartString.tr,
        title: StringsManager.warningString.tr,
        lang: language,
      );
    } else {
      isLoading.value = true;
      favoriteRepository.insertProductIntoFavorites(favoritesModel: favoritesModel!).then((value) {
        isLoading.value = false;
        favorites.add(value);
      }).catchError((error) {
        print(error.toString());
        isLoading.value = false;
      });
    }
  }

  getAllProductsInFavorites() async {
    isLoading.value = true;
    await favoriteRepository.getAllProductsFromFavorites().then((value) {
      favorites.addAll(value);
      updateProductsFavorites();
    });
    isLoading.value = false;
  }

  deleteProductFromFavorites({required int id, required String language}) async {
    isLoading.value = true;
    await favoriteRepository.deleteProductFromFavorites(id: id).then((value) {
      isLoading.value = false;
      favorites.removeWhere((element) => element.id == id);
      updateProductsFavorites();
      BaseToast.showDone(
        lang: language,
        title: StringsManager.successString.tr,
        msg: StringsManager.successDeleteProductFromFavorites.tr,
      );
    });
    isLoading.value = false;
    update();
  }

  bool checkIfProductsIsExistedInFavorites(int id) {
    return favorites.any((p0) => p0.productId == id);
  }
}