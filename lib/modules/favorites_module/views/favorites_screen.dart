import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/favorite_repository/favorite_repository.dart';
import 'package:shop_application/data/service/favorite_service/favorite_service.dart';
import 'package:shop_application/modules/favorites_module/components/favorite_item.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/widget_states/empty_widget_state/base_empty_widget.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';
import '../logic/controller/favorites_controller.dart';

class FavoritesScreen extends StatefulWidget {
  @override
  State<FavoritesScreen> createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  FavoritesController? favoritesController;
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
    favoritesController = Get.put(
      FavoritesController(
        favoriteRepository:
            FavoriteRepository(favoriteService: FavoriteService.instance),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      page: Obx(
        () => favoritesController!.isLoading.value
            ? BaseLoading()
            : favoritesController!.favorites.isEmpty
                ? BaseEmptyWidget(language: languageController!.language)
                : SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        ListView.separated(
                          physics: const BouncingScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) => FavoriteItem(
                            favoriteProduct:
                                favoritesController!.favorites[index],
                            onTapFunction: () =>
                                favoritesController!.deleteProductFromFavorites(
                              id: favoritesController!.favorites[index].id!,
                              language: languageController!.language,
                            ),
                            language: languageController!.language,
                          ),
                          separatorBuilder: (context, index) => SizedBox(
                            height: getProportionateScreenHeight(SizeManager.s15),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(PaddingManager.p12),
                            vertical: getProportionateScreenHeight(PaddingManager.p15),
                          ),
                          itemCount: favoritesController!.favorites.length,
                        ),
                        const SizedBox(height: SizeManager.s50),
                      ],
                    ),
                  ),
      ),
      pageTitle: StringsManager.favoritesString.tr,
      isCategoryPage: false,
      isProductsPage: false,
      isProfilePage: false,
    );
  }
}
