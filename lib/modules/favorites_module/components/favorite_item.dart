import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/models/favorites/favorites_model.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

import '../../../shared/resourses/consts_manager.dart';
import '../../../shared/config/size_config.dart';
import '../../../shared/widgets/base/base_network_image.dart';

class FavoriteItem extends StatelessWidget {
  final FavoritesModel favoriteProduct;
  final void Function()? onTapFunction;
  final String language;

  const FavoriteItem(
      {required this.favoriteProduct,
      required this.onTapFunction,
      required this.language});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Stack(
          alignment: AlignmentDirectional.bottomStart,
          children: [
            SizedBox(
              width: getProportionateScreenHeight(SizeManager.s100),
              height: getProportionateScreenHeight(SizeManager.s100),
              child: BaseNetworkImage(
                imageColor: ColorsManager.transparentColor,
                imageHeight: getProportionateScreenHeight(SizeManager.s100),
                imageUrl: favoriteProduct.productImage,
                imageBoxFit: BoxFit.cover,
              ),
            ),
            if (favoriteProduct.productDiscount != 0)
              Container(
                color: ColorsManager.kDiscountColor,
                child: Padding(
                  padding: const EdgeInsets.all(PaddingManager.p8),
                  child: Text(
                    StringsManager.discountString.tr,
                    style: language == ConstsManager.en
                        ? getRegularTextStyleBaloo2(
                            textColor: ColorsManager.kWhiteColor,
                            fontSize: FontSizeManager.size10,
                          )
                        : getRegularTextStyleTajawal(
                            textColor: ColorsManager.kWhiteColor,
                            fontSize: FontSizeManager.size10,
                          ),
                  ),
                ),
              )
          ],
        ),
        SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                favoriteProduct.productName,
                maxLines: SizeManager.s3.toInt(),
                overflow: TextOverflow.ellipsis,
                style: language == ConstsManager.en
                    ? getRegularTextStyleBaloo2(
                        textColor: ColorsManager.dividerColor,
                        fontSize: FontSizeManager.size16,
                      )
                    : getRegularTextStyleTajawal(
                        textColor: ColorsManager.dividerColor,
                        fontSize: FontSizeManager.size16,
                      ),
              ),
              SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
              Row(
                children: [
                  Row(
                    children: [
                      const Center(
                        child: BaseIcon(
                          iconColor: ColorsManager.kBlackColor,
                          iconHeight: SizeManager.s12,
                          iconPath: IconManager.dollarIcon,
                        ),
                      ),
                      SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
                      Text(favoriteProduct.productPrice.round().toString(),
                          style: getBoldTextStyleBaloo2(
                            textColor: ColorsManager.kBlackColor,
                            fontSize: FontSizeManager.size12,
                          )),
                    ],
                  ),
                  SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
                  if (favoriteProduct.productDiscount != 0)
                    Text(
                      favoriteProduct.productOldPrice.round().toString(),
                      style: getBoldTextStyleBaloo2(
                        textColor: ColorsManager.kBlackColor,
                        fontSize: FontSizeManager.size12,
                        textDecoration: TextDecoration.lineThrough,
                      ),
                    ),
                ],
              )
            ],
          ),
        ),
        const Spacer(),
        GestureDetector(
          onTap: onTapFunction,
          child: const BaseIcon(
            iconColor: ColorsManager.deleteColor,
            iconHeight: SizeManager.s20,
            iconPath: IconManager.trashIcon,
          ),
        ),
      ],
    );
  }
}
