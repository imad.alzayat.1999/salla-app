import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/favorite_repository/favorite_repository.dart';
import 'package:shop_application/data/service/favorite_service/favorite_service.dart';
import 'package:shop_application/modules/favorites_module/logic/controller/favorites_controller.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../../models/products/home_model.dart';
import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/widgets/icons/base_icon.dart';
import '../../details_module/views/details_screen.dart';


class ProductItem extends StatefulWidget {
  final Product product;
  final String language;

  const ProductItem({
    Key? key,
    required this.product,
    required this.language,
  }) : super(key: key);

  @override
  State<ProductItem> createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  FavoritesController? favoritesController;

  @override
  void initState() {
    super.initState();
    favoritesController = Get.put(
      FavoritesController(
        favoriteRepository:
            FavoriteRepository(favoriteService: FavoriteService.instance),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => GestureDetector(
          onTap: () {
            Get.to(DetailsScreen(product: widget.product));
          },
          child: Column(
            children: [
              Stack(
                alignment: AlignmentDirectional.bottomStart,
                children: [
                  BaseNetworkImage(
                    imageUrl: widget.product.image,
                    imageHeight: getProportionateScreenHeight(SizeManager.s150),
                    imageBoxFit: BoxFit.cover,
                    imageColor: ColorsManager.transparentColor,
                  ),
                  if (widget.product.discount != 0)
                    Container(
                      color: ColorsManager.kDiscountColor,
                      child: Padding(
                        padding: const EdgeInsets.all(PaddingManager.p8),
                        child: Text(
                          StringsManager.discountString.tr,
                          style: widget.language == ConstsManager.en
                              ? getRegularTextStyleBaloo2(
                                  textColor: ColorsManager.kWhiteColor,
                                )
                              : getRegularTextStyleTajawal(
                                  textColor: ColorsManager.kWhiteColor,
                                ),
                        ),
                      ),
                    )
                ],
              ),
              Text(
                widget.product.name,
                maxLines: SizeManager.s1.toInt(),
                style: widget.language == ConstsManager.en
                    ? getRegularTextStyleBaloo2(
                        textColor: ColorsManager.kBlackColor,
                        fontSize: FontSizeManager.size14,
                        textOverflow: TextOverflow.ellipsis,
                      )
                    : getRegularTextStyleTajawal(
                        textColor: ColorsManager.kBlackColor,
                        fontSize: FontSizeManager.size14,
                        textOverflow: TextOverflow.ellipsis),
              ),
              Row(children: [
                Text(
                  "${widget.product.price.round().toString()} \$",
                  style: getRegularTextStyleBaloo2(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size12,
                  ),
                ),
                SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
                if (widget.product.discount != 0)
                  Text("${widget.product.oldPrice.round().toString()} \$",
                      style: getRegularTextStyleBaloo2(
                        textColor: ColorsManager.kBlackColor,
                        fontSize: FontSizeManager.size10,
                        textDecoration: TextDecoration.lineThrough,
                      )),
                const Spacer(),
                favoritesController!.isLoading.value
                    ? BaseLoading()
                    : IconButton(
                        onPressed: () =>
                            favoritesController!.addProductToFavorites(
                          language: widget.language,
                          productId: widget.product.id,
                          productName: widget.product.name,
                          productDescription: widget.product.description,
                          productImage: widget.product.image,
                          productOldPrice: widget.product.oldPrice,
                          productPrice: widget.product.price,
                          productDiscount: widget.product.discount,
                        ),
                        icon: BaseIcon(
                          iconColor: favoritesController!.checkIfProductsIsExistedInFavorites(widget.product.id)
                              ? ColorsManager.kEnabledButtonColor
                              : ColorsManager.kDisabledButtonColor,
                          iconHeight: SizeManager.s20,
                          iconPath: IconManager.favoriteIcon,
                        ),
                      ),
              ]),
            ],
          ),
        ));
  }
}
