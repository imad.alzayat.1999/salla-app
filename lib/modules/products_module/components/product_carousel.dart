import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:shop_application/models/products/home_model.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';

class ProductCarousel extends StatelessWidget {
  final HomeData homeData;

  ProductCarousel({required this.homeData});

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      items: homeData.banners
          .map(
            (banner) => BaseNetworkImage(
              imageUrl: banner.image,
              imageHeight: getProportionateScreenHeight(SizeManager.s170),
              imageBoxFit: BoxFit.cover,
              imageColor: ColorsManager.transparentColor,
            ),
          )
          .toList(),
      options: CarouselOptions(
        height: getProportionateScreenHeight(SizeManager.s170),
        enableInfiniteScroll: true,
        viewportFraction: 1.0,
        reverse: false,
        initialPage: 0,
        autoPlay: true,
        autoPlayInterval: const Duration(
          seconds: ConstsManager.durationInSecondsInterval,
        ),
        autoPlayAnimationDuration: const Duration(
          seconds: ConstsManager.durationInSecondsCarouselAnimation,
        ),
        autoPlayCurve: Curves.fastOutSlowIn,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
