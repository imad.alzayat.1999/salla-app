import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/products_module/components/product_carousel.dart';
import 'package:shop_application/modules/products_module/logic/home_bloc.dart';
import 'package:shop_application/modules/products_module/logic/home_states.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';
import 'category_slider.dart';
import 'grid_of_products.dart';

class ProductsPage extends StatefulWidget {
  final String language;

  const ProductsPage({Key? key, required this.language}) : super(key: key);

  @override
  _ProductsPageState createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  @override
  void initState() {
    super.initState();
    print(widget.language);
    BlocProvider.of<HomeBloc>(context).getHomeData(language: widget.language);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeStates>(
      builder: (context, states) {
        if (states is HomeLoadingState) {
          return BaseLoading();
        }
        return SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(PaddingManager.p10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ProductCarousel(homeData: HomeBloc.get(context).homeData!),
                SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
                Text(
                  StringsManager.categoryString.tr,
                  style: widget.language == ConstsManager.en
                      ? getBoldTextStyleBaloo2(
                          textColor: ColorsManager.dividerColor,
                          fontSize: FontSizeManager.size20,
                        )
                      : getBoldTextStyleTajawal(
                          textColor: ColorsManager.dividerColor,
                          fontSize: FontSizeManager.size20,
                        ),
                ),
                CategorySlider(lang: widget.language),
                SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
                Text(
                  StringsManager.productsString.tr,
                  style: widget.language == ConstsManager.en
                      ? getBoldTextStyleBaloo2(
                          textColor: ColorsManager.dividerColor,
                          fontSize: FontSizeManager.size20,
                        )
                      : getBoldTextStyleTajawal(
                          textColor: ColorsManager.dividerColor,
                          fontSize: FontSizeManager.size20,
                        ),
                ),
                GridOfProducts(
                  homeData: HomeBloc.get(context).homeData!,
                  lang: widget.language,
                ),
                const SizedBox(height: SizeManager.s50),
              ],
            ),
          ),
        );
      },
    );
  }
}
