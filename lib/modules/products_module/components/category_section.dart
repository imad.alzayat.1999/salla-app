import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/models/category/category_model.dart';
import 'package:shop_application/modules/categories_module/logic/bloc/category_bloc.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../categories_module/logic/states/category_states.dart';
import 'category_item.dart';

class CategorySection extends StatefulWidget {
  final String language;

  const CategorySection({Key? key, required this.language}) : super(key: key);

  @override
  _CategorySectionState createState() => _CategorySectionState();
}

class _CategorySectionState extends State<CategorySection> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<CategoryBloc>(context)
        .fetchCategoryData(language: widget.language);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryStates>(
      builder: (context, state) {
        List<Category> categories = [];
        if (state is CategoryLoadingState) {
          return BaseLoading();
        }
        if (state is CategorySuccessState) {
          categories = state.categories;
        }
        return SizedBox(
          height: getProportionateScreenHeight(SizeManager.s100),
          child: ListView.separated(
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => CategoryItem(
              category: categories[index],
              language: widget.language,
            ),
            separatorBuilder: (context, index) =>
                SizedBox(width: getProportionateScreenWidth(SizeManager.s10)),
            itemCount: categories.length,
          ),
        );
      },
    );
  }
}
