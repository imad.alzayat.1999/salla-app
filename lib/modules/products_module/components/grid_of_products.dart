import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/models/products/home_model.dart';
import 'package:shop_application/modules/details_module/views/details_screen.dart';
import 'package:shop_application/modules/products_module/components/product_item.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class GridOfProducts extends StatelessWidget {
  final HomeData homeData;
  final String lang;

  const GridOfProducts({
    required this.homeData,
    required this.lang,
  });

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      crossAxisCount: 2,
      mainAxisSpacing: 10,
      crossAxisSpacing: 10,
      childAspectRatio: 1 / 1.6,
      children: List.generate(
        homeData.products.length,
        (index) => ProductItem(
          product: homeData.products[index],
          language: lang,
        ),
      ),
    );
  }
}
