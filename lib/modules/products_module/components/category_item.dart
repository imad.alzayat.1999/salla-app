import 'package:flutter/material.dart';
import 'package:shop_application/models/category/category_model.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';

class CategoryItem extends StatelessWidget {
  final Category category;
  final String language;

  const CategoryItem({
    required this.category,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getProportionateScreenHeight(SizeManager.s100),
      width: getProportionateScreenHeight(SizeManager.s100),
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          BaseNetworkImage(
            imageUrl: category.image,
            imageHeight: getProportionateScreenHeight(SizeManager.s100),
            imageBoxFit: BoxFit.cover,
            imageColor: ColorsManager.transparentColor,
          ),
          Container(
            color: ColorsManager.kCategoriesBackgroundColor.withOpacity(0.8),
            width: double.infinity,
            padding: const EdgeInsets.all(PaddingManager.p5),
            child: Text(
              category.name,
              style: language == ConstsManager.en
                  ? getBoldTextStyleBaloo2(textColor: ColorsManager.kBlackColor)
                  : getBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor),
              textAlign: TextAlign.center,
              maxLines: SizeManager.s1.toInt(),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
