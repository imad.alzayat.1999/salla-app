import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/category_repository/category_repository.dart';
import 'package:shop_application/data/service/category_service/category_service.dart';
import 'package:shop_application/models/category/category_model.dart';
import 'package:shop_application/modules/categories_module/logic/bloc/category_bloc.dart';
import 'package:shop_application/modules/categories_module/logic/states/category_states.dart';
import 'package:shop_application/modules/products_module/components/category_section.dart';
import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';
import 'category_item.dart';

class CategorySlider extends StatelessWidget {
  final String lang;

  const CategorySlider({required this.lang});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CategoryBloc(
        CategoryRepository(
          CategoryService(),
        ),
      ),
      child: CategorySection(language: lang),
    );
  }
}
