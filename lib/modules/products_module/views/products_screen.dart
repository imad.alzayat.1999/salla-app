import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/home_repository/home_repository.dart';
import 'package:shop_application/data/service/home_service/home_service.dart';
import 'package:shop_application/modules/products_module/components/products_page.dart';
import 'package:shop_application/modules/products_module/logic/home_bloc.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import '../../../shared/config/size_config.dart';

class ProductScreen extends StatefulWidget {
  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      isProfilePage: false,
      isProductsPage: true,
      isCategoryPage: false,
      pageTitle: StringsManager.homePageString.tr,
      page: BlocProvider(
        create: (context) => HomeBloc(
          homeRepository: HomeRepository(
            homeService: HomeService(),
          ),
        ),
        child: ProductsPage(language: languageController!.language),
      ),
    );
  }
}
