import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/home_repository/home_repository.dart';
import 'package:shop_application/models/products/home_model.dart';
import 'package:shop_application/modules/products_module/logic/home_states.dart';

class HomeBloc extends Cubit<HomeStates>{
  HomeBloc({required this.homeRepository}) : super(HomeInitState());

  final HomeRepository homeRepository;
  HomeData? homeData;

  static HomeBloc get(context) => BlocProvider.of<HomeBloc>(context);

  getHomeData({required String language}){
    emit(HomeLoadingState());
    homeRepository.getHomeDataRepository(language: language).then((value){
      homeData = HomeData.fromJson(value);
      emit(HomeSuccessState(homeData!));
    }).catchError((error){
      print(error.toString());
      emit(HomeFailedState());
    });
  }

}