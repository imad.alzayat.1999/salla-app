import '../../../models/products/home_model.dart';

abstract class HomeStates{

}

class HomeInitState extends HomeStates{}

class HomeLoadingState extends HomeStates{}

class HomeSuccessState extends HomeStates{
  final HomeData homeData;

  HomeSuccessState(this.homeData);
}

class HomeFailedState extends HomeStates{}