import 'package:flutter/material.dart';
import 'package:shop_application/models/splash/on_board_model.dart';
import 'package:shop_application/modules/login_module/view/login_screen.dart';
import 'package:shop_application/modules/on_board_module/components/on_board_item.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:get/get.dart';

import '../../../shared/config/size_config.dart';

class OnBoardScreen extends StatefulWidget {
  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  var pageController = PageController();
  List<OnBoardModel> items = [
    OnBoardModel(
      ImageManager.marketImage,
      StringsManager.emptyCartString.tr,
      StringsManager.firstQuestionString.tr,
    ),
    OnBoardModel(
      ImageManager.walletImage,
      StringsManager.walletString.tr,
      StringsManager.secondQuestionString.tr,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(PaddingManager.p20),
        child: Column(
          children: [
            Expanded(
              child: PageView.builder(
                physics: const BouncingScrollPhysics(),
                controller: pageController,
                itemBuilder: (context, index) {
                  return OnBoardItem(
                    image: items[index].image,
                    body: items[index].body,
                    title: items[index].title,
                    language: Get.deviceLocale!.languageCode,
                  );
                },
                itemCount: items.length,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(PaddingManager.p8),
                vertical: getProportionateScreenHeight(PaddingManager.p12),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SmoothPageIndicator(
                    controller: pageController,
                    count: items.length,
                    effect: const ExpandingDotsEffect(
                      dotColor: ColorsManager.kUnActivatedIndicatorColor,
                      activeDotColor: ColorsManager.kActivatedIndicatorColor,
                      dotHeight: 10,
                      dotWidth: 10,
                      spacing: 4,
                      expansionFactor: 3,
                    ),
                  ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(SizeManager.s12)),
                    onPressed: () => Get.to(LoginScreen()),
                    child: Row(
                      children: [
                        const BaseIcon(
                          iconHeight: SizeManager.s15,
                          iconPath: IconManager.skipIcon,
                          iconColor: ColorsManager.kBlackColor,
                        ),
                        const SizedBox(width: SizeManager.s10),
                        Text(
                          StringsManager.skipString.tr,
                          style:
                              Get.deviceLocale!.languageCode == ConstsManager.en
                                  ? getRegularTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                    )
                                  : getRegularTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                    ),
                        ),
                      ],
                    ),
                    color: ColorsManager.skipButtonColor,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
