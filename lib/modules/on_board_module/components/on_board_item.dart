import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

class OnBoardItem extends StatelessWidget {
  final String image;
  final String title;
  final String body;
  final String language;

  OnBoardItem({
    required this.image,
    required this.title,
    required this.body,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: SvgPicture.asset(image)),
        SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
        Text(
          title,
          style: language == ConstsManager.en
              ? getBoldTextStyleBaloo2(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size24,
                )
              : getBoldTextStyleTajawal(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size24,
                ),
        ),
        SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
        Text(
          body,
          style: language == ConstsManager.en
              ? getRegularTextStyleBaloo2(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size20,
                )
              : getRegularTextStyleTajawal(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size20,
                ),
        ),
      ],
    );
  }
}
