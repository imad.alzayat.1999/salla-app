import 'package:shop_application/models/change_password/change_password_model.dart';

abstract class ChangePasswordState{}

class ChangePasswordInitState extends ChangePasswordState{}

class ChangePasswordLoadingState extends ChangePasswordState{}

class ChangePasswordSuccessState extends ChangePasswordState{
  final ChangePasswordModel changePasswordModel;

  ChangePasswordSuccessState({required this.changePasswordModel});
}

class ChangePasswordFailedState extends ChangePasswordState{
  final String errorMessage;

  ChangePasswordFailedState({required this.errorMessage});
}