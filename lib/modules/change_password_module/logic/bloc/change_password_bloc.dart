import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/change_password_repository/change_password_repository.dart';
import 'package:shop_application/models/change_password/change_password_model.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';

import '../state/change_password_states.dart';

class ChangePasswordBloc extends Cubit<ChangePasswordState> {
  final ChangePasswordRepository changePasswordRepository;

  ChangePasswordBloc({required this.changePasswordRepository})
      : super(ChangePasswordInitState());

  static ChangePasswordBloc get(context) =>
      BlocProvider.of<ChangePasswordBloc>(context);
  ChangePasswordModel? changePasswordModel;

  changePassword({
    required String language,
    required String currentPassword,
    required String newPassword,
  }) {
    emit(ChangePasswordLoadingState());
    changePasswordRepository
        .changePassword(
      language: language,
      currentPassword: currentPassword,
      newPassword: newPassword,
    )
        .then((value) {
      changePasswordModel = value;
      emit(
        ChangePasswordSuccessState(changePasswordModel: changePasswordModel!),
      );
    }).catchError((error) {
      print(error.toString());
      emit(
        ChangePasswordFailedState(
          errorMessage: StringsManager.errorChangePasswordString.tr,
        ),
      );
    });
  }
}
