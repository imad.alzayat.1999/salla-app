import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/change_password_repository/change_password_repository.dart';
import 'package:shop_application/data/service/change_password_service/change_password_service.dart';
import 'package:shop_application/modules/change_password_module/logic/bloc/change_password_bloc.dart';
import 'package:shop_application/modules/change_password_module/logic/state/change_password_states.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_background_image.dart';
import 'package:shop_application/shared/widgets/base/base_text_field.dart';
import 'package:shop_application/shared/widgets/base/base_toast.dart';

import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/resourses/fonts_manager.dart';
import '../../../shared/resourses/styles_manager/styles_baloo2_manager.dart';
import '../../../shared/resourses/styles_manager/styles_tajawal_manager.dart';
import '../../../shared/resourses/validation_manager.dart';
import '../../../shared/widgets/base/base_button.dart';
import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  var globalKey = GlobalKey<FormState>();
  var currentPasswordController = TextEditingController();
  var newPasswordController = TextEditingController();
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ChangePasswordBloc(
        changePasswordRepository: ChangePasswordRepository(
          changePasswordService: ChangePasswordService(),
        ),
      ),
      child: BlocConsumer<ChangePasswordBloc, ChangePasswordState>(
        listener: (context, state) {
          if (state is ChangePasswordSuccessState) {
            if (state.changePasswordModel.status) {
              BaseToast.showDone(
                msg: state.changePasswordModel.message,
                title: StringsManager.successString.tr,
                lang: languageController!.language,
              );
              Future.delayed(
                  const Duration(
                    milliseconds: ConstsManager.durationInMillisecondsToast,
                  ), () {
                Get.back();
              });
            } else {
              BaseToast.showError(
                msg: state.changePasswordModel.message,
                title: StringsManager.errorString.tr,
                lang: languageController!.language,
              );
            }
          }
          if (state is ChangePasswordFailedState) {
            BaseToast.showError(
              msg: state.errorMessage,
              title: StringsManager.errorString.tr,
              lang: languageController!.language,
            );
          }
        },
        builder: (context, state) {
          return BaseApplicationPage(
            onPressFunction: () {},
            pageTitle: StringsManager.changePasswordString.tr,
            page: Stack(
              children: [
                BaseBackgroundImage(),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(PaddingManager.p15),
                  ),
                  child: Form(
                    key: globalKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          StringsManager.currentPasswordString.tr,
                          style:
                              languageController!.language == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                        ),
                        SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                        BaseTextField(
                          textInputType: TextInputType.text,
                          obsText: true,
                          controller: currentPasswordController,
                          iconData: IconManager.passwordIcon,
                          isPassword: true,
                          onSubmit: (String value) {},
                          onValidate: (String? currPass) {
                            if (!ValidationManager.isPasswordValid(currPass!)) {
                              return StringsManager.validationPassString.tr;
                            }
                            return null;
                          },
                          language: languageController!.language,
                        ),
                        SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                        Text(
                          StringsManager.newPasswordString.tr,
                          style:
                              languageController!.language == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                        ),
                        SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                        BaseTextField(
                          textInputType: TextInputType.text,
                          obsText: true,
                          controller: newPasswordController,
                          iconData: IconManager.passwordIcon,
                          isPassword: true,
                          onSubmit: (String value) {},
                          onValidate: (String? newPass) {
                            if (!ValidationManager.isPasswordValid(newPass!)) {
                              return StringsManager.validationPassString.tr;
                            }
                            return null;
                          },
                          language: languageController!.language,
                        ),
                        SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                        Center(
                          child: state is ChangePasswordLoadingState
                              ? BaseLoading()
                              : BaseButton(
                                  btnTextColor: ColorsManager.kBlackColor,
                                  language: languageController!.language,
                                  btnText:
                                      StringsManager.changePasswordString.tr,
                                  onPress: () {
                                    if (globalKey.currentState!.validate()) {
                                      globalKey.currentState!.save();
                                      ChangePasswordBloc.get(context)
                                          .changePassword(
                                        currentPassword:
                                            currentPasswordController.text,
                                        newPassword: newPasswordController.text,
                                        language: languageController!.language,
                                      );
                                    }
                                  },
                                  btnColor:
                                      ColorsManager.kChangePassBtnColor,
                                ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            isProductsPage: false,
            isCategoryPage: false,
            isProfilePage: false,
          );
        },
      ),
    );
  }
}
