import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

import '../../../models/language/language_model.dart';
import '../../../shared/resourses/values_manager.dart';
import '../../../shared/config/size_config.dart';

class LanguageItem extends StatelessWidget {
  final LanguageModel languageModel;
  final String language;

  const LanguageItem({
    Key? key,
    required this.languageModel,
    required this.language,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: PaddingManager.p10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(
            height: getProportionateScreenHeight(SizeManager.s18),
            width: getProportionateScreenWidth(SizeManager.s18),
            child: Center(
              child: Image.asset(
                languageModel.langImage,
              ),
            ),
          ),
          SizedBox(width: getProportionateScreenWidth(SizeManager.s10)),
          Expanded(
            child: Text(
              languageModel.langTitle.tr,
              style: language == ConstsManager.en
                  ? getRegularTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getRegularTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
          ),
          BaseIcon(
            iconHeight: SizeManager.s15,
            iconPath: IconManager.checkMarkIcon,
            iconColor: languageModel.langSelected
                ? ColorsManager.kBlackColor
                : ColorsManager.kWhiteColor,
          ),
        ],
      ),
    );
  }
}
