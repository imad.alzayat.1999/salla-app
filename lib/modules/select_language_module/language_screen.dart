import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/on_board_module/view/on_board_screen.dart';
import 'package:shop_application/shared/widgets/base/base_background_image.dart';
import 'package:shop_application/shared/widgets/base/base_button.dart';
import '../../models/language/language_model.dart';
import '../../shared/language/controller/language_controller.dart';
import '../../shared/resourses/assets_manager.dart';
import '../../shared/resourses/colors_manager.dart';
import '../../shared/resourses/consts_manager.dart';
import '../../shared/resourses/strings_manager.dart';
import '../../shared/resourses/values_manager.dart';
import '../../shared/config/size_config.dart';
import '../../shared/widgets/language/base_expanded_language_list.dart';
import '../../shared/widgets/language/base_language_button.dart';
import 'component/language_item.dart';

class SelectLanguageScreen extends StatefulWidget {
  const SelectLanguageScreen({Key? key}) : super(key: key);

  @override
  _SelectLanguageScreenState createState() => _SelectLanguageScreenState();
}

class _SelectLanguageScreenState extends State<SelectLanguageScreen> {
  bool isStrechted = true;
  String title = 'Select Language';
  List<LanguageModel> langs = <LanguageModel>[];
  LanguageController? languageController;


  @override
  void initState() {
    super.initState();
    langs.add(LanguageModel(
      langTitle: StringsManager.englishTitleValueString.tr,
      langSelected: false,
      langImage: IconManager.englishIcon,
      langValue: "en",
    ));
    langs.add(LanguageModel(
      langTitle: StringsManager.arabicTitleValueString.tr,
      langSelected: false,
      langImage: IconManager.arabicIcon,
      langValue: "ar",
    ));
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsManager.kWhiteColor,
      body: Stack(
        children: [
          BaseBackgroundImage(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BaseLanguageButton(
                onPressFunction: () {
                  setState(() {
                    isStrechted = !isStrechted;
                  });
                },
                title: title.tr,
                isExpanded: isStrechted,
                language: languageController!.language,
              ),
              const SizedBox(height: SizeManager.s15),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(PaddingManager.p80),
                ),
                child: BaseExpandedLanguageList(
                  expansionDuration: const Duration(
                    milliseconds: ConstsManager.durationInMilliseconds,
                  ),
                  expand: isStrechted,
                  child: Container(
                    width: getProportionateScreenWidth(SizeManager.s216),
                    height:
                    getProportionateScreenHeight(SizeManager.s100),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        SizeManager.s20,
                      ),
                      color: ColorsManager.kWhiteColor,
                    ),
                    child: ListView.separated(
                      separatorBuilder: (context, index) => const Divider(
                        thickness: SizeManager.s1,
                        color: ColorsManager.dividerColor,
                      ),
                      itemCount: langs.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.only(
                        top: languageController!.language == ConstsManager.en
                            ? getProportionateScreenHeight(PaddingManager.p12)
                            : getProportionateScreenHeight(PaddingManager.p18),
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              langs.forEach(
                                    (element) => element.langSelected = false,
                              );
                              langs[index].langSelected = true;
                              Future.delayed(
                                  const Duration(
                                    milliseconds: ConstsManager
                                        .durationInMilliseconds3,
                                  ), () {
                                setState(() {
                                  languageController!.changeLanguage(langs[index].langValue);
                                  title = langs[index].langTitle.tr;
                                });
                              });
                              Future.delayed(
                                  const Duration(
                                    milliseconds: ConstsManager
                                        .durationInMilliseconds4,
                                  ), () {
                                setState(() {
                                  isStrechted = false;
                                });
                              });
                            });
                          },
                          child: LanguageItem(
                            language: languageController!.language,
                            languageModel: langs[index],
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(height: getProportionateScreenHeight(SizeManager.s20)),
              BaseButton(
                language: languageController!.language,
                btnTextColor: ColorsManager.kWhiteColor,
                btnText: StringsManager.goToOnBoardString.tr,
                onPress: () => Get.to(OnBoardScreen()),
                btnColor: ColorsManager.goToOnBoardBtnColor,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
