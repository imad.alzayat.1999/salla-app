import 'package:flutter/material.dart';
import 'package:shop_application/modules/details_module/components/product_add_to_cart.dart';
import 'package:shop_application/modules/details_module/components/small_image_preview.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';

class ProductImage extends StatefulWidget {
  final String image;
  final List<String> images;
  final String language;
  final int productId;
  final String productName;
  final String productDescription;
  final double productOldPrice;
  final double productPrice;
  final int productDiscount;

  const ProductImage(
      {required this.image,
      required this.images,
      required this.language,
      required this.productDiscount,
      required this.productPrice,
      required this.productOldPrice,
      required this.productDescription,
      required this.productName,
      required this.productId});

  @override
  State<ProductImage> createState() => _ProductImageState();
}

class _ProductImageState extends State<ProductImage> {
  int selectedImage = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          alignment: Alignment.topLeft,
          children: [
            SizedBox(
              width: getProportionateScreenWidth(double.infinity),
              height: getProportionateScreenHeight(SizeManager.s340),
              child: BaseNetworkImage(
                imageBoxFit: BoxFit.cover,
                imageColor: ColorsManager.transparentColor,
                imageHeight: getProportionateScreenHeight(SizeManager.s340),
                imageUrl: widget.images[selectedImage],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(PaddingManager.p10),
              ),
              child: ProductAddToCart(
                language: widget.language,
                productId: widget.productId,
                productName: widget.productName,
                productDescription: widget.productDescription,
                productImage: widget.image,
                productOldPrice: widget.productOldPrice,
                productPrice: widget.productPrice,
                productDiscount: widget.productDiscount,
              ),
            ),
          ],
        ),
        SizedBox(
          width: getProportionateScreenWidth(double.infinity),
          height: getProportionateScreenHeight(SizeManager.s50),
          child: Center(
            child: ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) => SmallImagePreview(
                onClickFunction: () {
                  setState(() {
                    selectedImage = index;
                  });
                },
                index: index,
                selectedImage: selectedImage,
                productSelectedImage: widget.images[index],
              ),
              separatorBuilder: (context, index) => SizedBox(
                width: getProportionateScreenWidth(
                  SizeManager.s10,
                ),
              ),
              itemCount: widget.images.length,
            ),
          ),
        )
      ],
    );
  }
}
