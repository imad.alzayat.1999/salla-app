import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';

import '../../../shared/config/size_config.dart';

class SmallImagePreview extends StatelessWidget {
  final void Function()? onClickFunction;
  final int selectedImage;
  final int index;
  final String productSelectedImage;

  const SmallImagePreview({
    required this.onClickFunction,
    required this.selectedImage,
    required this.index,
    required this.productSelectedImage,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClickFunction,
      child: AnimatedContainer(
        duration: const Duration(
          milliseconds: ConstsManager.durationInMillisecondsInPreviewImages,
        ),
        margin: EdgeInsets.only(right: getProportionateScreenWidth(MarginManager.m15)),
        padding: const EdgeInsets.all(PaddingManager.p8),
        height: getProportionateScreenHeight(SizeManager.s50),
        width: getProportionateScreenWidth(SizeManager.s50),
        decoration: BoxDecoration(
          color: ColorsManager.kWhiteColor,
          borderRadius: BorderRadius.circular(SizeManager.s10),
          border: Border.all(
            color: ColorsManager.kBlackColor.withOpacity(
              selectedImage == index ? SizeManager.s1 : SizeManager.s0_3,
            ),
          ),
        ),
        child: BaseNetworkImage(
          imageUrl: productSelectedImage,
          imageHeight: getProportionateScreenHeight(SizeManager.s50),
          imageColor: ColorsManager.transparentColor,
          imageBoxFit: BoxFit.cover,
        ),
      ),
    );
  }
}
