import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../../shared/resourses/values_manager.dart';

class ProductDescription extends StatelessWidget {
  final String description;
  final String language;

  const ProductDescription({
    required this.description,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(
          PaddingManager.p15,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            StringsManager.detailsDescriptionString.tr,
            style: language == ConstsManager.en
                ? getBoldTextStyleBaloo2(
                    textColor: ColorsManager.kDescriptionColor,
                    fontSize: FontSizeManager.size22,
                  )
                : getBoldTextStyleTajawal(
                    textColor: ColorsManager.kDescriptionColor,
                    fontSize: FontSizeManager.size22,
                  ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(
              SizeManager.s8,
            ),
          ),
          Text(
            description,
            style: language == ConstsManager.en
                ? getRegularTextStyleBaloo2(
                    textColor: ColorsManager.kDescriptionColor,
                    fontSize: FontSizeManager.size16,
                  )
                : getRegularTextStyleTajawal(
                    textColor: ColorsManager.kDescriptionColor,
                    fontSize: FontSizeManager.size16,
                  ),
          ),
        ],
      ),
    );
  }
}
