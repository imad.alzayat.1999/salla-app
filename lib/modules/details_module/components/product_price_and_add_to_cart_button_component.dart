import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import '../../../shared/widgets/icons/base_icon.dart';

class ProductPriceAndAddToCartButtonComponent extends StatelessWidget {
  final double price;
  final double oldPrice;
  final int discount;
  final String language;

  ProductPriceAndAddToCartButtonComponent({
    required this.price,
    required this.oldPrice,
    required this.discount,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          StringsManager.detailsPricingsString.tr,
          style: language == ConstsManager.en
              ? getBoldTextStyleBaloo2(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size16,
                )
              : getBoldTextStyleTajawal(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size16,
                ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Text(price.toString(),
                    style: getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size20,
                    )),
                const BaseIcon(
                  iconColor: ColorsManager.kIconColor,
                  iconHeight: SizeManager.s15,
                  iconPath: IconManager.dollarIcon,
                )
              ],
            ),
            if (discount != 0)
              Row(
                children: [
                  Text(
                    oldPrice.toString(),
                    style: getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kDiscountColor,
                      fontSize: FontSizeManager.size20,
                      textDecoration: TextDecoration.lineThrough,
                    ),
                  ),
                  const BaseIcon(
                    iconColor: ColorsManager.kIconColor,
                    iconHeight: SizeManager.s15,
                    iconPath: IconManager.dollarIcon,
                  )
                ],
              ),
          ],
        ),
      ],
    );
  }
}
