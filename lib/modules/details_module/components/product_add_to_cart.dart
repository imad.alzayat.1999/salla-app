import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/cart_repository/cart_repository.dart';
import 'package:shop_application/data/service/cart_service/cart_service.dart';
import 'package:shop_application/modules/cart_module/logic/controller/cart_controller.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';

import '../../../shared/resourses/assets_manager.dart';
import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/resourses/values_manager.dart';
import '../../../shared/widgets/icons/base_icon.dart';

class ProductAddToCart extends StatefulWidget {
  final String language;
  final int productId;
  final String productName;
  final String productDescription;
  final String productImage;
  final double productOldPrice;
  final double productPrice;
  final int productDiscount;

  const ProductAddToCart(
      {Key? key,
      required this.language,
      required this.productId,
      required this.productName,
      required this.productDescription,
      required this.productImage,
      required this.productOldPrice,
      required this.productPrice,
      required this.productDiscount})
      : super(key: key);

  @override
  State<ProductAddToCart> createState() => _ProductAddToCartState();
}

class _ProductAddToCartState extends State<ProductAddToCart> {
  CartController? _cartController;

  @override
  void initState() {
    super.initState();
    _cartController = Get.put(
      CartController(
        cartRepository: CartRepository(
          cartService: CartService.instance,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => _cartController!.isLoading.value
          ? BaseLoading()
          : SizedBox(
              height: getProportionateScreenHeight(SizeManager.s50),
              width: getProportionateScreenHeight(SizeManager.s50),
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(SizeManager.s200),
                ),
                onPressed: () => _cartController!.addProductToCart(
                  productId: widget.productId,
                  quantity: 1,
                  productName: widget.productName,
                  productDescription: widget.productDescription,
                  productImage: widget.productImage,
                  productOldPrice: widget.productOldPrice,
                  productPrice: widget.productPrice,
                  productDiscount: widget.productDiscount,
                  language: widget.language,
                ),
                color: _cartController!
                        .checkIfProductsIsExistedInTheCart(widget.productId)
                    ? ColorsManager.kAddedToCartColor
                    : ColorsManager.kAddToCartColor,
                child: BaseIcon(
                  iconPath: _cartController!
                          .checkIfProductsIsExistedInTheCart(widget.productId)
                      ? IconManager.addedToCartIcon
                      : IconManager.addToCartIcon,
                  iconHeight: SizeManager.s25,
                  iconColor: ColorsManager.kIconColor,
                ),
              ),
            ),
    );
  }
}
