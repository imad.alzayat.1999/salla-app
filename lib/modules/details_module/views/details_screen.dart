import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/models/products/home_model.dart';
import 'package:shop_application/modules/details_module/components/product_description.dart';
import 'package:shop_application/modules/details_module/components/product_image.dart';
import 'package:shop_application/modules/details_module/components/product_price_and_add_to_cart_button_component.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import '../../../shared/base_application_page/base_application_page.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';

class DetailsScreen extends StatefulWidget {
  final Product product;

  DetailsScreen({required this.product});

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      pageTitle: widget.product.name,
      onPressFunction: (){},
      isProfilePage: false,
      isProductsPage: false,
      isCategoryPage: false,
      page: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            ProductImage(
              image: widget.product.image,
              images: widget.product.images,
              language: languageController!.language,
              productId: widget.product.id,
              productDescription: widget.product.description,
              productDiscount: widget.product.discount,
              productName: widget.product.name,
              productOldPrice: widget.product.oldPrice,
              productPrice: widget.product.price,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(PaddingManager.p20),
                  child: Text(
                    widget.product.name,
                    style: languageController!.language == ConstsManager.en
                        ? getBoldTextStyleBaloo2(
                            textColor: ColorsManager.kBlackColor,
                            fontSize: FontSizeManager.size20,
                          )
                        : getBoldTextStyleTajawal(
                            textColor: ColorsManager.kBlackColor,
                            fontSize: FontSizeManager.size20,
                          ),
                  ),
                ),
                SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
                ProductDescription(
                  description: widget.product.description,
                  language: languageController!.language,
                ),
                SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(PaddingManager.p15),
                  ),
                  child: ProductPriceAndAddToCartButtonComponent(
                    language: languageController!.language,
                    price: widget.product.price,
                    oldPrice: widget.product.oldPrice,
                    discount: widget.product.discount,
                  ),
                )
              ],
            ),
            const SizedBox(height: SizeManager.s50),
          ],
        ),
      ),
    );
  }
}
