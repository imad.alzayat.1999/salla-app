import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/profile_repository/profile_repository.dart';
import 'package:shop_application/data/service/profile_service/profile_service.dart';
import 'package:shop_application/modules/profile_info_screen/components/profile_information_component.dart';
import 'package:shop_application/modules/profile_info_screen/logic/bloc/profile_info_bloc.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';

import '../../../models/profile/profile_model.dart';
import '../logic/states/profile_info_states.dart';

class ProfileInfoScreen extends StatefulWidget {
  const ProfileInfoScreen({Key? key}) : super(key: key);

  @override
  _ProfileInfoScreenState createState() => _ProfileInfoScreenState();
}

class _ProfileInfoScreenState extends State<ProfileInfoScreen> {
  LanguageController? _languageController;
  Profile? _profile;

  @override
  void initState() {
    super.initState();
    _languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: () {},
      pageTitle: StringsManager.profileInfoString.tr,
      page: BlocProvider(
        create: (context) => ProfileInfoBloc(
          profileRepository: ProfileRepository(
            profileService: ProfileService(),
          ),
        )..fetchUserInformation(language: _languageController!.language),
        child: BlocBuilder<ProfileInfoBloc, ProfileInfoStates>(
          builder: (context, state) {
            if (state is ProfileInfoLoadingState) {
              return BaseLoading();
            }
            if (state is ProfileInfoSuccessState) {
              _profile = state.profile;
            }
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ProfileInformationComponent(
                  infoIconPath: IconManager.userIcon,
                  infoTitle: StringsManager.nameString.tr,
                  infoContent: _profile!.name,
                  language: _languageController!.language,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(PaddingManager.p20),
                  ),
                  child: Divider(
                    color: ColorsManager.dividerColor,
                    thickness: SizeManager.s1,
                    height: getProportionateScreenHeight(SizeManager.s1),
                  ),
                ),
                ProfileInformationComponent(
                  infoIconPath: IconManager.emailIcon,
                  infoTitle: StringsManager.emailString.tr,
                  infoContent: _profile!.email,
                  language: _languageController!.language,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(PaddingManager.p20),
                  ),
                  child: Divider(
                    color: ColorsManager.dividerColor,
                    thickness: SizeManager.s1,
                    height: getProportionateScreenHeight(SizeManager.s1),
                  ),
                ),
                ProfileInformationComponent(
                  infoIconPath: IconManager.phoneIcon,
                  infoTitle: StringsManager.phoneString.tr,
                  infoContent: _profile!.phone,
                  language: _languageController!.language,
                ),
              ],
            );
          },
        ),
      ),
      isProductsPage: false,
      isCategoryPage: false,
      isProfilePage: true,
    );
  }
}
