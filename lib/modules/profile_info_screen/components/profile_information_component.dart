import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class ProfileInformationComponent extends StatelessWidget {
  final String infoIconPath;
  final String infoTitle;
  final String infoContent;
  final String language;

  const ProfileInformationComponent(
      {Key? key,
      required this.infoIconPath,
      required this.infoTitle,
      required this.infoContent,
      required this.language})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: getProportionateScreenHeight(PaddingManager.p10),
        horizontal: getProportionateScreenWidth(PaddingManager.p12),
      ),
      child: ListTile(
        leading: BaseIcon(
          iconColor: ColorsManager.kIconColor,
          iconHeight: SizeManager.s25,
          iconPath: infoIconPath,
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              infoTitle,
              style: language == ConstsManager.en
                  ? getSemiBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size18,
                    )
                  : getSemiBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size18,
                    ),
            ),
            SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
            Text(
              infoContent,
              style: language == ConstsManager.en
                  ? getRegularTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size14,
                    )
                  : getRegularTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size14,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
