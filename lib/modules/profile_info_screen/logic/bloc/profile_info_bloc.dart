import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/profile_repository/profile_repository.dart';
import 'package:shop_application/models/profile/profile_model.dart';
import 'package:shop_application/modules/profile_module/logic/states/profile_states.dart';

import '../states/profile_info_states.dart';

class ProfileInfoBloc extends Cubit<ProfileInfoStates> {
  ProfileInfoBloc({required this.profileRepository}) : super(ProfileInfoInitialState());

  static ProfileInfoBloc get(context) => BlocProvider.of<ProfileInfoBloc>(context);
  ProfileRepository profileRepository;
  Profile? profile;
  File? imageFile;

  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();

  void fetchUserInformation({required String language}) {
    emit(ProfileInfoLoadingState());
    profileRepository.getProfileInformation(language: language).then((value) {
      profile = value;
      nameController.text = profile!.name;
      emailController.text = profile!.email;
      phoneController.text = profile!.phone;
      emit(ProfileInfoSuccessState(profile: profile!));
    }).catchError((error) {
      print(error.toString());
      emit(ProfileInfoFailedState(error.toString()));
    });
  }
}
