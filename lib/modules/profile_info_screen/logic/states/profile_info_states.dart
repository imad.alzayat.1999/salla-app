import '../../../../models/profile/profile_model.dart';

abstract class ProfileInfoStates {}

class ProfileInfoInitialState extends ProfileInfoStates {}

class ProfileInfoLoadingState extends ProfileInfoStates {}

class ProfileInfoSuccessState extends ProfileInfoStates {
  final Profile profile;

  ProfileInfoSuccessState({required this.profile});
}

class ProfileInfoFailedState extends ProfileInfoStates {
  final String error;

  ProfileInfoFailedState(this.error);
}