import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/contact_info_module/views/contact_info_screen.dart';
import 'package:shop_application/modules/faq_module/view/faq_screen.dart';
import 'package:shop_application/modules/language_module/view/language_screen.dart';
import 'package:shop_application/modules/settings_module/components/settings_sections.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import '../../../shared/config/size_config.dart';

class SettingsView extends StatefulWidget {
  @override
  State<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  LanguageController? languageController;
  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      isProfilePage: false,
      isProductsPage: false,
      isCategoryPage: false,
      pageTitle: StringsManager.settingsString.tr,
      page: Column(
        children: [
          SettingsSections(
            iconPath: IconManager.faqIcon,
            title: StringsManager.faqString.tr,
            language: languageController!.language,
            onTapFunction: () => Get.to(FaqScreen()),
          ),
          SettingsSections(
            iconPath: IconManager.languageIcon,
            title: StringsManager.languageString.tr,
            language: languageController!.language,
            onTapFunction: () =>
                Get.to(LanguageScreen()),
          ),
          SettingsSections(
            iconPath: IconManager.contactsIcon,
            title: StringsManager.contactsString.tr,
            language: languageController!.language,
            onTapFunction: () => Get.to(ContactInfoScreen()),
          ),
        ],
      ),
    );
  }
}
