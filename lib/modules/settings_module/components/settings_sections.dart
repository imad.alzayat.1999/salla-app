import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import '../../../shared/widgets/icons/base_icon.dart';

class SettingsSections extends StatelessWidget {
  final String iconPath;
  final String title;
  final String language;
  final void Function()? onTapFunction;

  const SettingsSections({
    Key? key,
    required this.iconPath,
    required this.title,
    required this.language,
    required this.onTapFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: BaseIcon(
        iconColor: ColorsManager.kIconColor,
        iconHeight: SizeManager.s25,
        iconPath: iconPath,
      ),
      title: Text(
        title,
        style: language == ConstsManager.en
            ? getRegularTextStyleBaloo2(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size20,
              )
            : getRegularTextStyleTajawal(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size20,
              ),
      ),
      trailing: IconButton(
        onPressed: onTapFunction,
        icon: Icon(
          language == ConstsManager.en
              ? Icons.keyboard_arrow_right
              : Icons.keyboard_arrow_left,
          size: 40,
          color: ColorsManager.kIconColor,
        ),
      ),
    );
  }
}
