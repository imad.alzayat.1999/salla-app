import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/search_repository/search_repository.dart';
import 'package:shop_application/modules/search_module/components/search_page.dart';
import 'package:shop_application/modules/search_module/logic/bloc/search_bloc.dart';
import '../../../data/service/search_service/search_service.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';

class SearchScreen extends StatefulWidget {
  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BlocProvider(
      create: (context) => SearchBloc(
        searchRepository: SearchRepository(
          searchService: SearchService(),
        ),
      ),
      child: SearchPage(
        language: languageController!.language,
      ),
    );
  }
}
