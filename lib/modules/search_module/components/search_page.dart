import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/search_module/components/no_results.dart';
import 'package:shop_application/modules/search_module/components/search_here.dart';
import 'package:shop_application/modules/search_module/components/search_item.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

import '../../../shared/widgets/base/base_text_field.dart';
import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../logic/bloc/search_bloc.dart';
import '../logic/states/search_states.dart';

class SearchPage extends StatefulWidget {
  final String language;

  const SearchPage({Key? key, required this.language}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  var searchController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchStates>(
      builder: (context, states) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: ColorsManager.kWhiteColor,
            elevation: SizeManager.s0,
            leading: IconButton(
              onPressed: () => Get.back(),
              icon: BaseIcon(
                iconHeight: SizeManager.s20,
                iconColor: ColorsManager.kBlackColor,
                iconPath: IconManager.leftArrowIcon,
              ),
            ),
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(PaddingManager.p12),
                child: Column(
                  children: [
                    BaseTextField(
                      language: widget.language,
                      textInputType: TextInputType.text,
                      obsText: false,
                      controller: searchController,
                      iconData: IconManager.searchIcon,
                      onSubmit: (String text) {
                        SearchBloc.get(context).fetchSearchedProducts(
                          text: text,
                          language: widget.language,
                        );
                      },
                      onValidate: (String? name) {
                        print(name);
                      },
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(
                        SizeManager.s25,
                      ),
                    ),
                    if (states is SearchInitialState)
                      SearchHere(language: widget.language),
                    if (states is SearchLoadingState) BaseLoading(),
                    if (states is SearchSuccessState)
                      SearchBloc.get(context).searchedProducts.isEmpty
                          ? NoResults(language: widget.language)
                          : ListView.separated(
                              physics: const BouncingScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: SearchBloc.get(context)
                                  .searchedProducts
                                  .length,
                              separatorBuilder: (context, index) => SizedBox(
                                height: getProportionateScreenHeight(
                                  SizeManager.s10,
                                ),
                              ),
                              itemBuilder: (context, index) => SearchItem(
                                language: widget.language,
                                searchedProduct: SearchBloc.get(context)
                                    .searchedProducts[index],
                              ),
                            ),
                    const SizedBox(height: SizeManager.s50),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
