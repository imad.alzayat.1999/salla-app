import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../shared/animations/lottie_animations.dart';
import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/resourses/consts_manager.dart';
import '../../../shared/resourses/assets_manager.dart';
import '../../../shared/resourses/fonts_manager.dart';
import '../../../shared/resourses/strings_manager.dart';
import '../../../shared/resourses/styles_manager/styles_baloo2_manager.dart';
import '../../../shared/resourses/styles_manager/styles_tajawal_manager.dart';

class SearchHere extends StatelessWidget {
  final String language;

  const SearchHere({
    Key? key,
    required this.language,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const LottieAnimation(
            animationDuration: Duration(
              seconds: ConstsManager.durationInSecondLottieAnimation,
            ),
            animationFile: LottieManager.searchLottie,
          ),
          Text(
            StringsManager.searchHereString.tr,
            style: language == ConstsManager.en
                ? getSemiBoldTextStyleBaloo2(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size24,
                  )
                : getSemiBoldTextStyleTajawal(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size24,
                  ),
          ),
        ],
      ),
    );
  }
}
