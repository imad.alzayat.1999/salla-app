import 'package:flutter/material.dart';
import 'package:shop_application/models/search/search_model.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class SearchItem extends StatelessWidget {
  final SearchedProduct searchedProduct;
  final String language;

  SearchItem({
    required this.searchedProduct,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(SizeManager.s100),
          child: BaseNetworkImage(
            imageUrl: searchedProduct.image,
            imageHeight: getProportionateScreenHeight(SizeManager.s100),
            imageBoxFit: BoxFit.contain,
            imageColor: ColorsManager.transparentColor,
          ),
        ),
        SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                searchedProduct.name,
                maxLines: 3,
                style: language == ConstsManager.en
                    ? getRegularTextStyleBaloo2(
                        textColor: ColorsManager.kBlackColor,
                        fontSize: FontSizeManager.size14,
                        textOverflow: TextOverflow.ellipsis,
                      )
                    : getRegularTextStyleTajawal(
                        textColor: ColorsManager.kBlackColor,
                        fontSize: FontSizeManager.size14,
                        textOverflow: TextOverflow.ellipsis,
                      ),
              ),
              SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
              Row(
                children: [
                  Row(
                    children: [
                      const Center(
                        child: BaseIcon(
                          iconPath: IconManager.dollarIcon,
                          iconHeight: SizeManager.s12,
                          iconColor: ColorsManager.kIconColor,
                        ),
                      ),
                      SizedBox(width: getProportionateScreenWidth(SizeManager.s5)),
                      Text(
                        searchedProduct.price.round().toString(),
                        style: language == ConstsManager.en
                            ? getRegularTextStyleBaloo2(
                                textColor: ColorsManager.kBlackColor,
                              )
                            : getRegularTextStyleTajawal(
                                textColor: ColorsManager.kBlackColor,
                              ),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
