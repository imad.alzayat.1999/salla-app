import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/search_repository/search_repository.dart';
import 'package:shop_application/models/search/search_model.dart';
import 'package:shop_application/modules/search_module/logic/states/search_states.dart';

class SearchBloc extends Cubit<SearchStates> {
  SearchBloc({required this.searchRepository}) : super(SearchInitialState());

  static SearchBloc get(context) => BlocProvider.of<SearchBloc>(context);
  SearchRepository searchRepository;
  List<SearchedProduct> searchedProducts = [];

  void fetchSearchedProducts({required String text , required String language}) {
    emit(SearchLoadingState());
    searchRepository.getSearchedProductsRepository(language: language , inputText: text).then((value) {
      print("the searched products is: " + value.length.toString());
      searchedProducts = value;
      emit(SearchSuccessState(searchedProducts));
    }).catchError((error) {
      print(error.toString());
      emit(SearchFailedState(error.toString()));
    });
  }
}
