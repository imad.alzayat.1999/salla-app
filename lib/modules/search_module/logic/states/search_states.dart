import 'package:shop_application/models/search/search_model.dart';

abstract class SearchStates{}

class SearchInitialState extends SearchStates{}

class SearchLoadingState extends SearchStates{}

class SearchSuccessState extends SearchStates{
  final List<SearchedProduct> searchedProducts;

  SearchSuccessState(this.searchedProducts);

}

class SearchFailedState extends SearchStates{
  final String error;

  SearchFailedState(this.error);

}