import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/splash_module/splash_screen.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_background_image.dart';
import 'package:shop_application/shared/widgets/language/base_language_item.dart';
import '../../../models/language/language_model.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/language/language_switcher.dart';
import '../../../shared/widgets/language/base_expanded_language_list.dart';
import '../../../shared/widgets/language/base_language_button.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key? key}) : super(key: key);

  @override
  State<LanguageScreen> createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  bool isStrechted = false;
  String title = "";
  List<LanguageModel> langs = [];
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
    print("lang: " + languageController!.language);
    langs.add(LanguageModel(
      langTitle: "English",
      langSelected: languageController!.language == ConstsManager.en ? true : false,
      langImage: IconManager.englishIcon,
      langValue: "en",
    ));
    langs.add(LanguageModel(
      langTitle: "Arabic",
      langSelected: languageController!.language == ConstsManager.ar ? true : false,
      langImage: IconManager.arabicIcon,
      langValue: "ar",
    ));
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseApplicationPage(
      onPressFunction: (){},
      pageTitle: StringsManager.languageString.tr,
      page: Stack(
        children: [
          BaseBackgroundImage(),
          Positioned.fill(
            top: 0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BaseLanguageButton(
                  language: languageController!.language,
                  onPressFunction: () {
                    setState(() {
                      isStrechted = !isStrechted;
                    });
                  },
                  title: languageSwitcher(languageController!.language),
                  isExpanded: isStrechted,
                ),
                SizedBox(height: getProportionateScreenHeight(SizeManager.s10)),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(PaddingManager.p80),
                  ),
                  child: BaseExpandedLanguageList(
                    expansionDuration: const Duration(
                      milliseconds: ConstsManager.durationInMilliseconds,
                    ),
                    expand: isStrechted,
                    child: Container(
                      width: getProportionateScreenWidth(SizeManager.s216),
                      height: getProportionateScreenHeight(SizeManager.s80),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(SizeManager.s20),
                        color: ColorsManager.kLangDropDownBackgroundColor,
                      ),
                      child: ListView.separated(
                        separatorBuilder: (context, index) => const Divider(
                          thickness: SizeManager.s1,
                          color: ColorsManager.dividerColor,
                        ),
                        itemCount: langs.length,
                        shrinkWrap: true,
                        padding: EdgeInsets.only(
                          top: getProportionateScreenHeight(PaddingManager.p12),
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                langs.forEach(
                                  (element) => element.langSelected = false,
                                );
                                langs[index].langSelected = true;
                                languageController!
                                    .changeLanguage(langs[index].langValue);
                                Get.deleteAll(force: true);
                                Phoenix.rebirth(Get.context!);
                                Get.reset();
                                title = langs[index].langTitle.tr;
                                Future.delayed(
                                    const Duration(
                                      milliseconds:
                                          ConstsManager.durationInMilliseconds4,
                                    ), () {
                                  setState(() {
                                    isStrechted = false;
                                  });
                                });
                              });
                            },
                            child: BaseLanguageItem(
                              languageModel: langs[index],
                              language: languageController!.language,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      isProductsPage: false,
      isCategoryPage: false,
      isProfilePage: false,
    );
  }
}
