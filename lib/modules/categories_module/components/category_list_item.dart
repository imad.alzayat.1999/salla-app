import 'package:flutter/material.dart';
import 'package:shop_application/models/category/category_model.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';

import '../../../shared/resourses/colors_manager.dart';
import '../../../shared/widgets/base/base_network_image.dart';

class CategoryListItem extends StatelessWidget {
  final Category category;
  final String language;

  CategoryListItem({
    required this.category,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(PaddingManager.p12),
      child: Row(
        children: [
          SizedBox(
            width: getProportionateScreenWidth(SizeManager.s100),
            height: getProportionateScreenHeight(SizeManager.s100),
            child: BaseNetworkImage(
              imageColor: ColorsManager.transparentColor,
              imageHeight: getProportionateScreenHeight(SizeManager.s100),
              imageUrl: category.image,
              imageBoxFit: BoxFit.contain,
            ),
          ),
          SizedBox(width: getProportionateScreenWidth(SizeManager.s15)),
          Text(
            category.name,
            style: language == ConstsManager.en
                ? getBoldTextStyleBaloo2(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size20,
                  )
                : getBoldTextStyleTajawal(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size20,
                  ),
          ),
        ],
      ),
    );
  }
}
