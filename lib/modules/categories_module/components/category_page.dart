import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/models/category/category_model.dart';
import 'package:shop_application/modules/categories_module/logic/bloc/category_bloc.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../../shared/widgets/widget_states/empty_widget_state/base_empty_widget.dart';
import '../../../shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../logic/states/category_states.dart';
import 'category_list_item.dart';

class CategoryPage extends StatefulWidget {
  final String language;

  CategoryPage({Key? key, required this.language}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<CategoryBloc>(context)
        .fetchCategoryData(language: widget.language);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CategoryBloc, CategoryStates>(
      listener: (context, states) {},
      builder: (context, states) {
        List<Category> categories = [];
        if (states is CategoryLoadingState) {
          return BaseLoading();
        }
        if (states is CategorySuccessState) {
          categories = states.categories;
        }
        return categories.isEmpty
            ? BaseEmptyWidget(language: widget.language)
            : SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    ListView.separated(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (context, index) => CategoryListItem(
                        category: categories[index],
                        language: widget.language,
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(
                          PaddingManager.p12,
                        ),
                        vertical: getProportionateScreenHeight(
                          PaddingManager.p15,
                        ),
                      ),
                      separatorBuilder: (context, index) => SizedBox(
                        height: getProportionateScreenHeight(SizeManager.s5),
                      ),
                      itemCount: categories.length,
                    ),
                    SizedBox(height: SizeManager.s50),
                  ],
                ),
              );
      },
    );
  }
}
