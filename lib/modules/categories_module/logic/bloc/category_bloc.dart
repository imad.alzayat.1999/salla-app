import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_application/data/repository/category_repository/category_repository.dart';
import 'package:shop_application/data/service/category_service/category_service.dart';
import 'package:shop_application/models/category/category_model.dart';
import 'package:shop_application/modules/categories_module/logic/states/category_states.dart';

class CategoryBloc extends Cubit<CategoryStates>{
  CategoryBloc(this.categoryRepository) : super(CategoryInitialState());

  CategoryRepository categoryRepository;
  List<Category> categories = [];

  void fetchCategoryData({required String language}) {
    emit(CategoryLoadingState());
    categoryRepository.getCategoriesRepository(language: language).then((value) {
      categories.addAll(value);
      emit(CategorySuccessState(categories));
    }).catchError((error) {
      print(error.toString());
      emit(CategoryFailedState(error.toString()));
    });
  }

}