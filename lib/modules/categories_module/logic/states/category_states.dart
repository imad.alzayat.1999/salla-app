import 'package:shop_application/models/category/category_model.dart';

abstract class CategoryStates{}

class CategoryInitialState extends CategoryStates{}

class CategoryLoadingState extends CategoryStates{}

class CategorySuccessState extends CategoryStates{
  final List<Category> categories;

  CategorySuccessState(this.categories);
}
class CategoryFailedState extends CategoryStates{
  final String error;

  CategoryFailedState(this.error);
}