import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/category_repository/category_repository.dart';
import 'package:shop_application/data/service/category_service/category_service.dart';
import 'package:shop_application/modules/categories_module/components/category_page.dart';
import 'package:shop_application/modules/categories_module/logic/bloc/category_bloc.dart';
import 'package:shop_application/shared/base_application_page/base_application_page.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import '../../../shared/language/controller/language_controller.dart';
import '../../../shared/config/size_config.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  State<CategoriesScreen> createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GetBuilder<LanguageController>(
      init: LanguageController(),
      builder: (controller) {
        return BaseApplicationPage(
          onPressFunction: () {},
          pageTitle: StringsManager.categoryString.tr,
          isCategoryPage: true,
          isProductsPage: false,
          isProfilePage: false,
          page: BlocProvider(
            create: (context) => CategoryBloc(
              CategoryRepository(CategoryService()),
            ),
            child: CategoryPage(
              language: languageController!.language,
            ),
          ),
        );
      },
    );
  }
}
