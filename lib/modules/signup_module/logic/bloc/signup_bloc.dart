import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/authentication_repository/register_repository.dart';
import 'package:shop_application/data/service/authentication_service/register_service.dart';
import 'package:shop_application/models/authentication/login_model.dart';
import 'package:shop_application/models/authentication/signup_model.dart';
import 'package:shop_application/modules/login_module/logic/state/login_states.dart';
import 'package:shop_application/modules/signup_module/logic/state/signup_states.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/network/end_points.dart';
import 'package:flutter/material.dart';

import '../../../../shared/resourses/strings_manager.dart';
import '../../../../shared/widgets/base/base_toast.dart';

class SignupBloc extends Cubit<SignupStates> {
  SignupBloc({required this.registerRepository}) : super(SignupInitialState());

  static SignupBloc get(context) => BlocProvider.of(context);
  SignupModel? signupModel;
  RegisterRepository registerRepository;

  void registerUser({
    required String email,
    required String password,
    required String name,
    required String phone,
    required String language,
  }) {
    emit(SignupLoadingState());
    registerRepository
        .registerAccountRepository(
            email: email,
            password: password,
            name: name,
            phone: phone,
            language: language)
        .then((value) {
      signupModel = SignupModel.fromJson(value);
      LocalStorage.putData(key: "password", value: password);
      emit(
        SignupSuccessState(signupModel: signupModel!),
      );
    }).catchError((error) {
      BaseToast.showError(
        lang: language,
        title: StringsManager.errorString.tr,
        msg: error.toString(),
      );
      emit(SignupFailedState(error.toString()));
    });
  }
}
