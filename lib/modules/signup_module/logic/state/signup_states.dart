import 'package:flutter/material.dart';
import 'package:shop_application/models/authentication/signup_model.dart';

abstract class SignupStates{}


class SignupInitialState extends SignupStates{}

class SignupLoadingState extends SignupStates{}

class SignupSuccessState extends SignupStates{
  final SignupModel signupModel;

  SignupSuccessState({required this.signupModel});
}

class SignupFailedState extends SignupStates{
  final String error;

  SignupFailedState(this.error);

}
