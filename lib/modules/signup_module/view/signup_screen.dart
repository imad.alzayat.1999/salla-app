import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/authentication_repository/register_repository.dart';
import 'package:shop_application/data/service/authentication_service/register_service.dart';
import 'package:shop_application/modules/products_module/views/products_screen.dart';
import 'package:shop_application/modules/signup_module/logic/bloc/signup_bloc.dart';
import 'package:shop_application/modules/signup_module/logic/state/signup_states.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/base/base_background_image.dart';
import 'package:shop_application/shared/widgets/base/base_button.dart';
import 'package:shop_application/shared/widgets/base/base_text_field.dart';
import 'package:shop_application/shared/widgets/base/base_toast.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../../shared/resourses/validation_manager.dart';
import '../../../shared/config/size_config.dart';

class SignupScreen extends StatefulWidget {
  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  var emailController = TextEditingController();
  var passController = TextEditingController();
  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  String? lang;

  @override
  void initState() {
    super.initState();
    lang = LocalStorage.getData(key: "lang");
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BlocProvider(
      create: (context) => SignupBloc(
        registerRepository: RegisterRepository(
          registerService: RegisterService(),
        ),
      ),
      child: BlocConsumer<SignupBloc, SignupStates>(listener: (context, state) {
        if (state is SignupSuccessState) {
          if (state.signupModel.status) {
            print(state.signupModel);
            LocalStorage.putData(key: "password", value: passController.text);
            LocalStorage.putData(
                    key: "token", value: state.signupModel.data.token)
                .then(
              (value) => Get.to(
                ProductScreen(),
              ),
            );
          } else {
            BaseToast.showError(
              msg: state.signupModel.message,
              title: StringsManager.errorString.tr,
              lang: lang!,
            );
          }
        }
      }, builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: Stack(
              children: [
                BaseBackgroundImage(),
                Center(
                  child: Form(
                    key: formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(PaddingManager.p12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              StringsManager.registerString.tr,
                              style: lang == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s25)),
                            Text(
                              StringsManager.emailString.tr,
                              style: lang == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            BaseTextField(
                              language: lang!,
                              textInputType: TextInputType.emailAddress,
                              obsText: false,
                              controller: emailController,
                              iconData: IconManager.emailIcon,
                              onSubmit: (String) {},
                              onValidate: (String? email) {
                                if (!ValidationManager.isEmailValid(email!)) {
                                  return StringsManager
                                      .validationEmailString.tr;
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                            Text(
                              StringsManager.passwordString.tr,
                              style: lang == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            BaseTextField(
                              language: lang!,
                              textInputType: TextInputType.visiblePassword,
                              isPassword: true,
                              obsText: true,
                              controller: passController,
                              iconData: IconManager.passwordIcon,
                              onSubmit: (String) {},
                              onValidate: (String? password) {
                                if (!ValidationManager.isPasswordValid(
                                    password!)) {
                                  return StringsManager.validationPassString.tr;
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                            Text(
                              StringsManager.nameString.tr,
                              style: lang == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            BaseTextField(
                              language: lang!,
                              textInputType: TextInputType.text,
                              obsText: false,
                              controller: nameController,
                              iconData: IconManager.userIcon,
                              onSubmit: (String) {},
                              onValidate: (String? name) {
                                if (!ValidationManager.isTextValid(name!)) {
                                  return StringsManager.validationNameString.tr;
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s30)),
                            Text(
                              StringsManager.phoneString.tr,
                              style: lang == ConstsManager.en
                                  ? getBoldTextStyleBaloo2(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    )
                                  : getBoldTextStyleTajawal(
                                      textColor: ColorsManager.kBlackColor,
                                      fontSize: FontSizeManager.size20,
                                    ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            BaseTextField(
                              language: lang!,
                              textInputType: TextInputType.phone,
                              obsText: false,
                              controller: phoneController,
                              iconData: IconManager.phoneIcon,
                              onSubmit: (String) {},
                              onValidate: (String? phone) {
                                if (!ValidationManager.isPhoneValid(phone!)) {
                                  return StringsManager
                                      .validationPhoneString.tr;
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: getProportionateScreenHeight(SizeManager.s15)),
                            state is SignupLoadingState
                                ? BaseLoading()
                                : Center(
                                    child: BaseButton(
                                      language: lang!,
                                      btnTextColor: ColorsManager.kBlackColor,
                                      btnText: StringsManager.registerString.tr,
                                      onPress: () {
                                        if (formKey.currentState!.validate()) {
                                          formKey.currentState!.save();
                                          SignupBloc.get(context).registerUser(
                                            email: emailController.text,
                                            password: passController.text,
                                            name: nameController.text,
                                            phone: phoneController.text,
                                            language: lang!,
                                          );
                                        }
                                      },
                                      btnColor:
                                          ColorsManager.kAuthenticationBtnColor,
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
