class FavoriteFields {
  static final List<String> values = [
    id,
    productId,
    productName,
    productPrice,
    productImage,
    productOldPrice,
    productDescription,
    productDiscount
  ];

  static const String id = 'id';
  static const String productId = "product_id";
  static const String productName = 'product_name';
  static const String productPrice = 'product_price';
  static const String productImage = 'product_image';
  static const String productOldPrice = "product_old_price";
  static const String productDescription = "product_description";
  static const String productDiscount = "product_discount";
  static const String userToken = "user_token";
}

class FavoritesModel {
  int? id;
  final int productId;
  final String productName;
  final double productPrice;
  final String productImage;
  final double productOldPrice;
  final String productDescription;
  final int productDiscount;
  final String userToken;

  FavoritesModel({
    this.id,
    required this.productId,
    required this.productName,
    required this.productDescription,
    required this.productImage,
    required this.productOldPrice,
    required this.productPrice,
    required this.productDiscount,
    required this.userToken,
  });

  static FavoritesModel fromJson(Map<String, dynamic> json) => FavoritesModel(
        id: json[FavoriteFields.id] as int,
        productId: json[FavoriteFields.productId],
        productName: json[FavoriteFields.productName],
        productImage: json[FavoriteFields.productImage],
        productPrice: json[FavoriteFields.productPrice],
        productDescription: json[FavoriteFields.productDescription],
        productOldPrice: json[FavoriteFields.productOldPrice],
        productDiscount: json[FavoriteFields.productDiscount],
        userToken: json[FavoriteFields.userToken],
      );

  Map<String, dynamic> toJson() => {
        FavoriteFields.id: id,
        FavoriteFields.productName: productName,
        FavoriteFields.productId: productId,
        FavoriteFields.productPrice: productPrice,
        FavoriteFields.productImage: productImage,
        FavoriteFields.productDescription: productDescription,
        FavoriteFields.productOldPrice: productOldPrice,
        FavoriteFields.productDiscount: productDiscount,
        FavoriteFields.userToken : userToken,
      };
}
