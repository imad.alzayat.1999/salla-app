import 'dart:convert';

import 'package:flutter/material.dart';

SignupModel signupModelFromJson(String str) => SignupModel.fromJson(json.decode(str));

String signupModelToJson(SignupModel data) => json.encode(data.toJson());

class SignupModel {
  SignupModel({
    required this.status,
    required this.message,
    required this.data,
  });

  bool status;
  String message;
  Data data;

  factory SignupModel.fromJson(Map<String, dynamic> json) => SignupModel(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.name,
    required this.email,
    required this.phone,
    required this.id,
    required this.image,
    required this.token,
  });

  String name;
  String email;
  String phone;
  int id;
  String image;
  String token;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    name: json["name"] as String,
    email: json["email"] as String,
    phone: json["phone"] as String,
    id: json["id"] as int,
    image: json["image"] as String,
    token: json["token"] as String,
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "phone": phone,
    "id": id,
    "image": image,
    "token": token,
  };
}
