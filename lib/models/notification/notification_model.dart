class NotificationModel {
  NotificationModel({
    required this.status,
    required this.message,
    required this.data,
  });

  bool status;
  dynamic message;
  Data data;

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.data,
    required this.total,
  });

  List<NotificationItem> data;
  int total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        data: List<NotificationItem>.from(
          json["data"].map(
            (x) => NotificationItem.fromJson(x),
          ),
        ),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class NotificationItem {
  NotificationItem({
    required this.id,
    required this.title,
    required this.message,
  });

  int id;
  String title;
  String message;

  factory NotificationItem.fromJson(Map<String, dynamic> json) => NotificationItem(
        id: json["id"],
        title: json["title"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "message": message,
      };
}
