class FaqModel {
  FaqModel({
    required this.status,
    required this.message,
    required this.data,
  });

  bool status;
  dynamic message;
  Data data;

  factory FaqModel.fromJson(Map<String, dynamic> json) => FaqModel(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.faqs,
  });

  List<FAQ> faqs;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    faqs: List<FAQ>.from(json["data"].map((x) => FAQ.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(faqs.map((x) => x.toJson())),
  };
}

class FAQ {
  FAQ({
    required this.id,
    required this.question,
    required this.answer,
  });

  int id;
  String question;
  String answer;

  factory FAQ.fromJson(Map<String, dynamic> json) => FAQ(
    id: json["id"],
    question: json["question"],
    answer: json["answer"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "question": question,
    "answer": answer,
  };
}
