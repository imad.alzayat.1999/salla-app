class CartFields {
  static final List<String> values = [
    id,
    productId,
    quantity,
    productName,
    productPrice,
    productImage,
    productOldPrice,
    productDescription,
    productDiscount,
    userToken,
  ];

  static const String id = 'id';
  static const String productId = "product_id";
  static const String quantity = 'quantity';
  static const String productName = 'product_name';
  static const String productPrice = 'product_price';
  static const String productImage = 'product_image';
  static const String productOldPrice = "product_old_price";
  static const String productDescription = "product_description";
  static const String productDiscount = "product_discount";
  static const String userToken = "user_token";
}

class CartModel {
  int? id;
  final int productId;
  int quantity;
  final String productName;
  final double productPrice;
  final String productImage;
  final double productOldPrice;
  final String productDescription;
  final int productDiscount;
  final String userToken;

  CartModel({
    this.id,
    required this.productId,
    required this.quantity,
    required this.productName,
    required this.productDescription,
    required this.productImage,
    required this.productOldPrice,
    required this.productPrice,
    required this.productDiscount,
    required this.userToken,
  });

  static CartModel fromJson(Map<String, dynamic> json) => CartModel(
        id: json[CartFields.id] as int,
        productId: json[CartFields.productId],
        quantity: json[CartFields.quantity],
        productName: json[CartFields.productName],
        productImage: json[CartFields.productImage],
        productPrice: json[CartFields.productPrice],
        productDescription: json[CartFields.productDescription],
        productOldPrice: json[CartFields.productOldPrice],
        productDiscount: json[CartFields.productDiscount],
        userToken: json[CartFields.userToken]
      );

  Map<String, dynamic> toJson() => {
        CartFields.id: id,
        CartFields.productName: productName,
        CartFields.productId: productId,
        CartFields.quantity: quantity,
        CartFields.productPrice: productPrice,
        CartFields.productImage: productImage,
        CartFields.productDescription: productDescription,
        CartFields.productOldPrice: productOldPrice,
        CartFields.productDiscount: productDiscount,
        CartFields.userToken : userToken,
      };
}
