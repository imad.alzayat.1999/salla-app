class LanguageModel {
  String langImage;
  String langTitle;
  String langValue;
  bool langSelected;

  LanguageModel({
    required this.langImage,
    required this.langTitle,
    required this.langValue,
    required this.langSelected,
  });
}
