import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';

import '../config/size_config.dart';

class LottieAnimation extends StatefulWidget {
  final Duration animationDuration;
  final String animationFile;

  const LottieAnimation({
    Key? key,
    required this.animationDuration,
    required this.animationFile,
  }) : super(key: key);

  @override
  _LottieAnimationState createState() => _LottieAnimationState();
}

class _LottieAnimationState extends State<LottieAnimation>
    with TickerProviderStateMixin {
  AnimationController? animationController;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: widget.animationDuration);
  }

  @override
  void dispose() {
    super.dispose();
    animationController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Lottie.asset(
        widget.animationFile,
        animate: true,
        width: getProportionateScreenWidth(double.infinity),
        height: getProportionateScreenHeight(SizeManager.s340),
      ),
    );
  }
}
