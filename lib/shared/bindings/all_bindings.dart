import 'package:get/get.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';

class AllBindings extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => LanguageController());
  }
}