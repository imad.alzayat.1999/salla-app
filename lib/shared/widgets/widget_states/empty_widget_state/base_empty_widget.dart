import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/animations/lottie_animations.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';

class BaseEmptyWidget extends StatelessWidget {
  final String language;

  const BaseEmptyWidget({
    Key? key,
    required this.language,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const LottieAnimation(
            animationDuration: Duration(
              seconds: ConstsManager.durationInSecondLottieAnimation,
            ),
            animationFile: LottieManager.emptyLottie,
          ),
          Text(
            StringsManager.nothingFoundString.tr,
            style: language == ConstsManager.en
                ? getSemiBoldTextStyleBaloo2(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size24,
                  )
                : getSemiBoldTextStyleTajawal(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size24,
                  ),
          ),
        ],
      ),
    );
  }
}
