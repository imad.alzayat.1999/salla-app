import 'package:flutter/material.dart';

class BaseIcon extends StatelessWidget {
  final double iconHeight;
  final String iconPath;
  final Color iconColor;

  const BaseIcon({
    Key? key,
    required this.iconHeight,
    required this.iconPath,
    required this.iconColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: iconHeight,
      child: Image.asset(
        iconPath,
        color: iconColor,
      ),
    );
  }
}
