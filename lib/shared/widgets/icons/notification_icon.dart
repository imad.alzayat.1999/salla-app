import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shop_application/data/repository/notification_repository/notification_repository.dart';
import 'package:shop_application/data/service/notification_service/notification_service.dart';
import 'package:shop_application/models/notification/notification_model.dart';
import 'package:shop_application/modules/notifications_module/logic/notification_bloc.dart';
import 'package:shop_application/modules/notifications_module/logic/notification_state.dart';
import 'package:shop_application/modules/notifications_module/view/notification_screen.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

import '../widget_states/loading_page_state/base_loading.dart';

class NotificationIcon extends StatelessWidget {
  final String language;

  const NotificationIcon({Key? key, required this.language}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => NotificationBloc(
        NotificationRepository(
          NotificationService(),
        ),
      )..fetchNotifications(language: language),
      child: BlocConsumer<NotificationBloc, NotificationState>(
        listener: (context, state) {},
        builder: (context, state) {
          List<NotificationItem> notifications = [];
          if (state is NotificationLoadingState) {
            return BaseLoading();
          }
          if (state is NotificationSuccessState) {
            notifications = state.notifications;
          }
          return Padding(
            padding: const EdgeInsets.all(PaddingManager.p10),
            child: GestureDetector(
              onTap: () {
                Get.to(NotificationScreen());
              },
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  const BaseIcon(
                    iconHeight: SizeManager.s35,
                    iconPath: IconManager.notificationIcon,
                    iconColor: ColorsManager.kIconColor,
                  ),
                  Positioned(
                    top: SizeManager.s0,
                    left: SizeManager.s12,
                    child: Container(
                      width: getProportionateScreenHeight(SizeManager.s10),
                      height: getProportionateScreenHeight(SizeManager.s10),
                      decoration: const BoxDecoration(
                        color: ColorsManager.kNotificationCardColor,
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
