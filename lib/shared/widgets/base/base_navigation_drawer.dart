import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/change_password_module/view/change_password_screen.dart';
import 'package:shop_application/modules/favorites_module/views/favorites_screen.dart';
import 'package:shop_application/modules/login_module/view/login_screen.dart';
import 'package:shop_application/modules/settings_module/view/settings_view.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/base/base_network_image.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class BaseNavigationDrawer extends StatelessWidget {
  final String language;

  const BaseNavigationDrawer({
    Key? key,
    required this.language,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          BaseNetworkImage(
            imageUrl: "https://cdn.pixabay.com/photo/2017/08/06/18/36/mall-2595002_960_720.jpg",
            imageHeight: getProportionateScreenHeight(SizeManager.s340),
            imageBoxFit: BoxFit.cover,
            imageColor: ColorsManager.transparentColor,
          ),
          ListTile(
            leading: const BaseIcon(
              iconColor: ColorsManager.kIconColor,
              iconHeight: SizeManager.s20,
              iconPath: IconManager.settingsIcon,
            ),
            title: Text(
              StringsManager.settingsString.tr,
              style: language == ConstsManager.en
                  ? getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
            onTap: () => Get.to(SettingsView()),
          ),
          ListTile(
            leading: const BaseIcon(
              iconPath: IconManager.favoriteIcon,
              iconColor: ColorsManager.kIconColor,
              iconHeight: SizeManager.s20,
            ),
            title: Text(
              StringsManager.favoritesString.tr,
              style: language == ConstsManager.en
                  ? getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
            onTap: () => Get.to(FavoritesScreen()),
          ),
          ListTile(
            leading: const BaseIcon(
              iconPath: IconManager.changePasswordIcon,
              iconColor: ColorsManager.kIconColor,
              iconHeight: SizeManager.s20,
            ),
            title: Text(
              StringsManager.changePasswordString.tr,
              style: language == ConstsManager.en
                  ? getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
            onTap: () => Get.to(ChangePasswordScreen()),
          ),
          ListTile(
            leading: const BaseIcon(
              iconPath: IconManager.logoutIcon,
              iconColor: ColorsManager.kIconColor,
              iconHeight: SizeManager.s20,
            ),
            title: Text(
              StringsManager.logoutString.tr,
              style: language == ConstsManager.en
                  ? getBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    )
                  : getBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                    ),
            ),
            onTap: () {
              LocalStorage.removeData(key: "token");
              Get.to(LoginScreen());
            },
          ),
        ],
      ),
    );
  }
}
