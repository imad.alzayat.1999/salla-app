import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';

class BaseAlertDialog extends StatefulWidget {
  final void Function()? onPressFunction;

  const BaseAlertDialog({
    Key? key,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  State<BaseAlertDialog> createState() => _BaseAlertDialogState();
}

class _BaseAlertDialogState extends State<BaseAlertDialog> {
  String? lang;

  @override
  void initState() {
    super.initState();
    lang = LocalStorage.getData(key: "lang");
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        StringsManager.deleteConfirmationString.tr,
        style: lang == ConstsManager.en
            ? getRegularTextStyleBaloo2(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              )
            : getRegularTextStyleTajawal(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              ),
      ),
      content: Text(
        StringsManager.areYouSureString.tr,
        style: lang == ConstsManager.en
            ? getRegularTextStyleBaloo2(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              )
            : getRegularTextStyleTajawal(
                textColor: ColorsManager.kBlackColor,
                fontSize: FontSizeManager.size16,
              ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: widget.onPressFunction,
          child: Text(
            StringsManager.deleteString.tr,
            style: lang == ConstsManager.en
                ? getRegularTextStyleBaloo2(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size16,
                  )
                : getRegularTextStyleTajawal(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size16,
                  ),
          ),
        ),
        TextButton(
          onPressed: () => Get.back(),
          child: Text(
            StringsManager.cancelString.tr,
            style: lang == ConstsManager.en
                ? getRegularTextStyleBaloo2(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size16,
                  )
                : getRegularTextStyleTajawal(
                    textColor: ColorsManager.kBlackColor,
                    fontSize: FontSizeManager.size16,
                  ),
          ),
        ),
      ],
    );
  }
}
