import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';

class BaseButton extends StatelessWidget {
  final String btnText;
  final void Function()? onPress;
  final Color btnColor;
  final Color btnTextColor;
  final String language;

  BaseButton({
    required this.btnText,
    required this.onPress,
    required this.btnColor,
    required this.btnTextColor,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPress,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeManager.s10)
      ),
      child: Text(
        btnText,
        style: language == ConstsManager.en
            ? getRegularTextStyleBaloo2(
                textColor: btnTextColor,
                fontSize: FontSizeManager.size14,
              )
            : getRegularTextStyleTajawal(
                textColor: btnTextColor,
                fontSize: FontSizeManager.size14,
              ),
      ),
      color: btnColor,
    );
  }
}
