import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../icons/base_icon.dart';
import 'base_network_image.dart';

class BaseProfileImage extends StatelessWidget {
  final Widget image;

  const BaseProfileImage({
    Key? key,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(double.infinity),
      height: getProportionateScreenHeight(SizeManager.s226),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          bottomRight: Radius.circular(SizeManager.s20),
          bottomLeft: Radius.circular(SizeManager.s20),
        ),
        child: BaseNetworkImage(
          imageUrl: ImageManager.avatarImage,
          imageHeight: getProportionateScreenHeight(
            SizeManager.s226,
          ),
          imageColor: ColorsManager.kWhiteColor,
          imageBoxFit: BoxFit.cover,
        ),
      ),
    );
  }
}
