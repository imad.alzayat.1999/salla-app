// ignore: file_names
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class BaseToast {
  static void show({
    required String message,
    required String language,
    required String title,
    SnackBarType status = SnackBarType.INFO,
  }) {
    Get.showSnackbar(
      GetSnackBar(
        message: message,
        title: title,
        messageText: Text(
          message,
          style: language == ConstsManager.en
              ? getRegularTextStyleBaloo2(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size16,
                )
              : getRegularTextStyleTajawal(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size16,
                ),
        ),
        titleText: Text(
          title,
          style: language == ConstsManager.en
              ? getBoldTextStyleBaloo2(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size20,
                )
              : getBoldTextStyleTajawal(
                  textColor: ColorsManager.kBlackColor,
                  fontSize: FontSizeManager.size20,
                ),
        ),
        snackPosition: status == SnackBarType.DONE
            ? SnackPosition.BOTTOM
            : SnackPosition.TOP,
        backgroundColor: getColorStatus(status),
        duration: const Duration(
          seconds: ConstsManager.durationInSecondsSnackBar,
        ),
        key: UniqueKey(),
        icon: BaseIcon(
          iconPath: getIconData(status),
          iconHeight: SizeManager.s20,
          iconColor: ColorsManager.kBlackColor,
        ),
        leftBarIndicatorColor: getColorStatus(status),
        progressIndicatorValueColor:
            AlwaysStoppedAnimation<Color>(getColorStatus(status)),
        showProgressIndicator: false,
        maxWidth: SizeManager.s600,
      ),
    );
  }

  static showInfo(
      {required String msg, required String title, required String lang}) {
    show(message: msg, status: SnackBarType.INFO, title: title, language: lang);
  }

  static showWarning(
      {required String msg, required String title, required String lang}) {
    show(
        message: msg,
        status: SnackBarType.WARNING,
        language: lang,
        title: title);
  }

  static showError(
      {required String msg, required String title, required String lang}) {
    show(
        message: msg, status: SnackBarType.ERROR, title: title, language: lang);
  }

  static showDone(
      {required String msg, required String title, required String lang}) {
    show(message: msg, status: SnackBarType.DONE, title: title, language: lang);
  }
}

Color getColorStatus(SnackBarType status) {
  switch (status) {
    case SnackBarType.INFO:
      return ColorsManager.infoSnackBarColor;
    case SnackBarType.WARNING:
      return ColorsManager.warningSnackBarColor;
    case SnackBarType.ERROR:
      return ColorsManager.failedSnackBarColor;
    case SnackBarType.DONE:
      return ColorsManager.successSnackBarColor;
    default:
      return ColorsManager.kWhiteColor;
  }
}

String getIconData(SnackBarType status) {
  switch (status) {
    case SnackBarType.INFO:
      return IconManager.infoIcon;
    case SnackBarType.WARNING:
      return IconManager.warningIcon;
    case SnackBarType.ERROR:
      return IconManager.failedIcon;
    case SnackBarType.DONE:
      return IconManager.doneIcon;
    default:
      return IconManager.infoIcon;
  }
}

enum SnackBarType {
  INFO,
  ERROR,
  WARNING,
  DONE,
}
