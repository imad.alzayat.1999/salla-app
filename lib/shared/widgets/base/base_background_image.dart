import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';

import '../../resourses/values_manager.dart';


class BaseBackgroundImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Opacity(
        opacity: SizeManager.s0_1,
        child: Container(
          height: getProportionateScreenHeight(double.infinity),
          width: getProportionateScreenWidth(double.infinity),
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(ImageManager.appLogo),
            ),
          ),
        ),
      ),
    );
  }
}
