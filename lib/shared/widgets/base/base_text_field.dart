import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class BaseTextField extends StatefulWidget {
  final TextInputType textInputType;
  bool obsText;
  final TextEditingController controller;
  final String iconData;
  final void Function(String)? onSubmit;
  final String? Function(String?)? onValidate;
  final String language;
  final bool isPassword;

  BaseTextField({
    required this.textInputType,
    required this.obsText,
    required this.controller,
    required this.iconData,
    required this.onSubmit,
    required this.onValidate,
    this.isPassword = false,
    required this.language,
  });

  @override
  State<BaseTextField> createState() => _BaseTextFieldState();
}

class _BaseTextFieldState extends State<BaseTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: widget.textInputType,
      obscureText: widget.obsText,
      controller: widget.controller,
      onFieldSubmitted: widget.onSubmit,
      validator: widget.onValidate,
      decoration: InputDecoration(
        errorStyle: widget.language == ConstsManager.en
            ? getRegularTextStyleBaloo2(textColor: ColorsManager.failedColor)
            : getRegularTextStyleTajawal(textColor: ColorsManager.failedColor),
        prefixIcon: Padding(
          padding: const EdgeInsets.all(PaddingManager.p12),
          child: BaseIcon(
            iconColor: ColorsManager.kIconColor,
            iconHeight: SizeManager.s20,
            iconPath: widget.iconData,
          ),
        ),
        suffixIcon: widget.isPassword
            ? IconButton(
                onPressed: () {
                  setState(() {
                    widget.obsText = !widget.obsText;
                  });
                },
                icon: Icon(
                  widget.obsText ? Icons.visibility_off : Icons.visibility,
                ),
              )
            : null,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(SizeManager.s10),
        ),
      ),
    );
  }
}
