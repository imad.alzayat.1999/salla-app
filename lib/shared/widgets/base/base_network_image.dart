import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shop_application/shared/widgets/widget_states/loading_page_state/base_loading.dart';
import '../../resourses/assets_manager.dart';
import '../../resourses/colors_manager.dart';
import '../../resourses/values_manager.dart';
import '../icons/base_icon.dart';

class BaseNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double imageHeight;
  final BoxFit imageBoxFit;
  final Color imageColor;

  BaseNetworkImage({
    required this.imageUrl,
    required this.imageHeight,
    required this.imageBoxFit,
    required this.imageColor,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      height: imageHeight,
      fit: imageBoxFit,
      color: imageColor,
      colorBlendMode: BlendMode.darken,
      placeholder: (context, url) => BaseLoading(),
      errorWidget: (context, url, error) => const BaseIcon(
        iconHeight: SizeManager.s20,
        iconPath: IconManager.failedIcon,
        iconColor: ColorsManager.failedColor,
      ),
    );
  }
}
