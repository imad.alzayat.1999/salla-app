import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';
import '../../../models/language/language_model.dart';
import '../../resourses/values_manager.dart';

class BaseLanguageItem extends StatelessWidget {
  final LanguageModel languageModel;
  final String language;

  const BaseLanguageItem({
    required this.languageModel,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: PaddingManager.p10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(
            height: getProportionateScreenHeight(SizeManager.s18),
            width: getProportionateScreenHeight(SizeManager.s18),
            child: Center(
              child: Image.asset(
                languageModel.langImage,
              ),
            ),
          ),
          const SizedBox(width: SizeManager.s10),
          Expanded(
            child: Text(
              languageModel.langTitle.tr,
              style: language == ConstsManager.en
                  ? getRegularTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size15,
                    )
                  : getRegularTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size15,
                    ),
            ),
          ),
          BaseIcon(
            iconHeight: SizeManager.s16,
            iconPath: IconManager.checkMarkIcon,
            iconColor: languageModel.langSelected
                ? ColorsManager.kBlackColor
                : ColorsManager.kWhiteColor,
          ),
        ],
      ),
    );
  }
}
