import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import '../icons/base_icon.dart';

class BaseLanguageButton extends StatelessWidget {
  final void Function()? onPressFunction;
  final String title;
  final bool isExpanded;
  final String language;

  BaseLanguageButton({
    required this.onPressFunction,
    required this.title,
    required this.isExpanded,
    required this.language,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(SizeManager.s226),
      height: getProportionateScreenHeight(SizeManager.s43),
      child: OutlinedButton(
        onPressed: onPressFunction,
        style: OutlinedButton.styleFrom(
          backgroundColor: Colors.transparent,
          side: const BorderSide(
            color: ColorsManager.kLanguageButtonColor,
            width: SizeManager.s2,
            style: BorderStyle.solid,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              SizeManager.s7,
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: language == ConstsManager.en
                  ? getRegularTextStyleBaloo2(
                      textColor: ColorsManager.textColor2,
                      fontSize: FontSizeManager.size18,
                    )
                  : getRegularTextStyleTajawal(
                      textColor: ColorsManager.textColor2,
                      fontSize: FontSizeManager.size18,
                    ),
            ),
            Container(
              width: getProportionateScreenWidth(SizeManager.s22),
              height: getProportionateScreenHeight(SizeManager.s22),
              decoration: BoxDecoration(
                color: ColorsManager.kLanguageButtonColor,
                borderRadius: BorderRadius.circular(
                  SizeManager.s7,
                ),
              ),
              child: Center(
                child: BaseIcon(
                  iconPath: isExpanded
                      ? IconManager.upArrowIcon
                      : IconManager.downArrowIcon,
                  iconColor: ColorsManager.kWhiteColor,
                  iconHeight: SizeManager.s18,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
