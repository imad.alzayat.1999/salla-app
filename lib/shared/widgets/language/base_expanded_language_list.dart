import 'package:flutter/material.dart';

class BaseExpandedLanguageList extends StatefulWidget {
  final Widget child;
  final bool expand;
  final Duration expansionDuration;

  BaseExpandedLanguageList({
    this.expand = false,
    required this.child,
    required this.expansionDuration,
  });

  @override
  _BaseExpandedLanguageListState createState() =>
      _BaseExpandedLanguageListState();
}

class _BaseExpandedLanguageListState extends State<BaseExpandedLanguageList>
    with SingleTickerProviderStateMixin {
  AnimationController? expandController;
  Animation<double>? animation;

  @override
  void initState() {
    super.initState();
    prepareAnimations();
    _runExpandCheck();
  }

  ///Setting up the animation
  void prepareAnimations() {
    expandController = AnimationController(
      vsync: this,
      duration: widget.expansionDuration,
    );
    animation = CurvedAnimation(
      parent: expandController!,
      curve: Curves.fastOutSlowIn,
    );
  }

  void _runExpandCheck() {
    if (widget.expand) {
      expandController!.forward();
    } else {
      expandController!.reverse();
    }
  }

  @override
  void didUpdateWidget(BaseExpandedLanguageList oldWidget) {
    super.didUpdateWidget(oldWidget);
    _runExpandCheck();
  }

  @override
  void dispose() {
    expandController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
      axisAlignment: 1.0,
      sizeFactor: animation!,
      child: Container(
        child: widget.child,
      ),
    );
  }
}
