import 'package:get/get.dart';

import 'languages/english.dart';
import 'languages/arabic.dart';


class GlobalTranslations extends Translations{
  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys => {
    'en' : english ,
    'ar' : arabic,
  };

}