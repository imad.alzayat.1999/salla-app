import 'package:get/get.dart';

String languageSwitcher(String lang) {
  switch (lang) {
    case "ar":
      return "Arabic".tr;
    case "en":
      return "English".tr;
    default:
      return "";
  }
}
