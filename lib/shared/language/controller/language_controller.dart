import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';

class LanguageController extends GetxController {
  String language = "en";

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    language = (LocalStorage.getData(key: "lang") == null
        ? Get.deviceLocale!.languageCode
        : LocalStorage.getData(key: "lang"))!;
    Get.updateLocale(Locale(language));
    print(language);
    update();
  }

  void changeLanguage(String type) async {
    if (language == type) {
      return;
    }
    if (type == 'en') {
      language = "en";
      LocalStorage.putData(key: "lang", value: language);
      Get.updateLocale(Locale(language));
      update();
    } else {
      language = "ar";
      LocalStorage.putData(key: "lang", value: language);
      Get.updateLocale(Locale(language));
      update();
    }
    update();
  }
}
