import 'package:flutter/material.dart';
import 'package:shop_application/models/products/home_model.dart';
import 'package:shop_application/modules/cart_module/view/cart_screen.dart';
import 'package:shop_application/modules/categories_module/views/categories_screen.dart';
import 'package:shop_application/modules/change_password_module/view/change_password_screen.dart';
import 'package:shop_application/modules/contact_info_module/views/contact_info_screen.dart';
import 'package:shop_application/modules/details_module/views/details_screen.dart';
import 'package:shop_application/modules/faq_module/view/faq_screen.dart';
import 'package:shop_application/modules/favorites_module/views/favorites_screen.dart';
import 'package:shop_application/modules/language_module/view/language_screen.dart';
import 'package:shop_application/modules/login_module/view/login_screen.dart';
import 'package:shop_application/modules/notifications_module/view/notification_screen.dart';
import 'package:shop_application/modules/on_board_module/view/on_board_screen.dart';
import 'package:shop_application/modules/products_module/views/products_screen.dart';
import 'package:shop_application/modules/profile_info_screen/view/profile_info_screen.dart';
import 'package:shop_application/modules/profile_module/views/profile_screen.dart';
import 'package:shop_application/modules/search_module/views/search_screen.dart';
import 'package:shop_application/modules/select_language_module/language_screen.dart';
import 'package:shop_application/modules/settings_module/view/settings_view.dart';
import 'package:shop_application/modules/signup_module/view/signup_screen.dart';
import 'package:shop_application/modules/splash_module/splash_screen.dart';
import 'package:shop_application/shared/resourses/routes_mananger.dart';

class RouteConfig {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutesManager.splashRoute:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case RoutesManager.loginRoute:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case RoutesManager.signupRoute:
        return MaterialPageRoute(builder: (_) => SignupScreen());
      case RoutesManager.onBoardRoute:
        return MaterialPageRoute(builder: (_) => OnBoardScreen());
      case RoutesManager.homeRoute:
        return MaterialPageRoute(builder: (_) => ProductScreen());
      case RoutesManager.profileRoute:
        return MaterialPageRoute(builder: (_) => ProfileScreen());
      case RoutesManager.notificationsRoute:
        return MaterialPageRoute(builder: (_) => NotificationScreen());
      case RoutesManager.languagesRoute:
        return MaterialPageRoute(builder: (_) => LanguageScreen());
      case RoutesManager.selectLanguageRoute:
        return MaterialPageRoute(builder: (_) => SelectLanguageScreen());
      case RoutesManager.favoritesRoute:
        return MaterialPageRoute(builder: (_) => FavoritesScreen());
      case RoutesManager.settingsRoute:
        return MaterialPageRoute(builder: (_) => SettingsView());
      case RoutesManager.searchRoute:
        return MaterialPageRoute(builder: (_) => SearchScreen());
      case RoutesManager.profileInfoRoute:
        return MaterialPageRoute(builder: (_) => ProfileInfoScreen());
      case RoutesManager.faqRoute:
        return MaterialPageRoute(builder: (_) => FaqScreen());
      case RoutesManager.detailsRoute:
        final args = settings.arguments as Product;
        return MaterialPageRoute(builder: (_) => DetailsScreen(product: args));
      case RoutesManager.contactInfoRoute:
        return MaterialPageRoute(builder: (_) => ContactInfoScreen());
      case RoutesManager.changePasswordRoute:
        return MaterialPageRoute(builder: (_) => ChangePasswordScreen());
      case RoutesManager.categoriesRoute:
        return MaterialPageRoute(builder: (_) => CategoriesScreen());
      case RoutesManager.cartRoute:
        return MaterialPageRoute(builder: (_) => CartScreen());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text("No Route fonud"),
            ),
          ),
        );
    }
  }
}
