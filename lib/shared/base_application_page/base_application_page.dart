import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/cart_module/view/cart_screen.dart';
import 'package:shop_application/modules/notifications_module/view/notification_screen.dart';
import 'package:shop_application/modules/profile_module/views/profile_screen.dart';
import 'package:shop_application/modules/search_module/views/search_screen.dart';
import 'package:shop_application/shared/bottom_navbar/bottom_navbar.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';
import 'package:shop_application/shared/widgets/base/base_navigation_drawer.dart';
import 'package:shop_application/shared/widgets/icons/notification_icon.dart';

class BaseApplicationPage extends StatefulWidget {
  final String pageTitle;
  final Widget page;
  final bool isProductsPage;
  final bool isCategoryPage;
  final bool isProfilePage;
  final bool isCartPage;
  final void Function()? onPressFunction;

  const BaseApplicationPage({
    Key? key,
    required this.pageTitle,
    required this.page,
    required this.isProductsPage,
    required this.isCategoryPage,
    required this.isProfilePage,
    this.isCartPage = false,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  State<BaseApplicationPage> createState() => _BaseApplicationPageState();
}

class _BaseApplicationPageState extends State<BaseApplicationPage> {
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorsManager.kWhiteColor,
        drawer: BaseNavigationDrawer(language: languageController!.language),
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: ColorsManager.kIconColor,
            size: SizeManager.s14,
          ),
          elevation: SizeManager.s0,
          backgroundColor: ColorsManager.kWhiteColor,
          actions: [
            IconButton(
              iconSize: SizeManager.s43,
              onPressed: () => Get.to(NotificationScreen()),
              icon: NotificationIcon(
                language: languageController!.language,
              ),
            ),
            IconButton(
              onPressed: () => Get.to(SearchScreen()),
              icon: const BaseIcon(
                iconPath: IconManager.searchIcon,
                iconColor: ColorsManager.kBlackColor,
                iconHeight: SizeManager.s20,
              ),
            ),
            widget.isProfilePage
                ? IconButton(
                    onPressed: () => Get.off(ProfileScreen()),
                    icon: const BaseIcon(
                      iconPath: IconManager.penIcon,
                      iconColor: ColorsManager.kBlackColor,
                      iconHeight: SizeManager.s20,
                    ),
                  )
                : SizedBox.shrink(),
            widget.isCartPage
                ? IconButton(
                    onPressed: widget.onPressFunction,
                    icon: const BaseIcon(
                      iconPath: IconManager.trashIcon,
                      iconColor: ColorsManager.kBlackColor,
                      iconHeight: SizeManager.s20,
                    ),
                  )
                : SizedBox.shrink()
          ],
          title: SizedBox(
            width: getProportionateScreenWidth(SizeManager.s200),
            child: Text(
              widget.pageTitle,
              style: languageController!.language == ConstsManager.en
                  ? getSemiBoldTextStyleBaloo2(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                      textOverflow: TextOverflow.ellipsis,
                    )
                  : getSemiBoldTextStyleTajawal(
                      textColor: ColorsManager.kBlackColor,
                      fontSize: FontSizeManager.size16,
                      textOverflow: TextOverflow.ellipsis,
                    ),
            ),
          ),
        ),
        body: widget.page,
        floatingActionButton: FloatingActionButton(
          onPressed: () => Get.to(CartScreen()),
          elevation: SizeManager.s4,
          backgroundColor: ColorsManager.kFABColor,
          child: const BaseIcon(
            iconPath: IconManager.cartIcon,
            iconHeight: SizeManager.s25,
            iconColor: ColorsManager.kIconColor,
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        bottomNavigationBar: BottomNavbar(
          language: languageController!.language,
          isCategoryActivated: widget.isCategoryPage,
          isProductsActivated: widget.isProductsPage,
          isProfileActivated: widget.isProfilePage,
        ));
  }
}
