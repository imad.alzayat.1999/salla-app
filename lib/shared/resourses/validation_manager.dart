class ValidationManager {
  static bool isPasswordValid(String password) => password.length >= 6;

  static bool isEmailValid(String email) {
    RegExp regex = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return regex.hasMatch(email);
  }

  static bool isTextValid(String inputText) =>
      inputText.isNotEmpty || inputText != "0";

  static bool isPhoneValid(String phone) => phone.length >= 8;
}
