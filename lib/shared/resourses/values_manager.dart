class SizeManager {
  static const double s0 = 0;
  static const double s0_1 = 0.1;
  static const double s0_3 = 0.3;
  static const double s0_5 = 0.5;
  static const double s0_6 = 0.6;
  static const double s1 = 1;
  static const double s2 = 2;
  static const double s3 = 3;
  static const double s4 = 4;
  static const double s5 = 5;
  static const double s7 = 7;
  static const double s8 = 8;
  static const double s10 = 10;
  static const double s12 = 12;
  static const double s14 = 14;
  static const double s15 = 15;
  static const double s16 = 16;
  static const double s18 = 18;
  static const double s20 = 20;
  static const double s22 = 22;
  static const double s25 = 25;
  static const double s30 = 30;
  static const double s35 = 35;
  static const double s43 = 43;
  static const double s50 = 50;
  static const double s60 = 60;
  static const double s80 = 80;
  static const double s100 = 100;
  static const double s117 = 117;
  static const double s127 = 127;
  static const double s150 = 150;
  static const double s170 = 170;
  static const double s200 = 200;
  static const double s216 = 216;
  static const double s226 = 226;
  static const double s340 = 340;
  static const double s600 = 600;
}

class PaddingManager {
  static const double p5 = 5;
  static const double p8 = 8;
  static const double p10 = 10;
  static const double p12 = 12;
  static const double p15 = 15;
  static const double p18 = 18;
  static const double p20 = 20;
  static const double p80 = 80;
}

class MarginManager {
  static const double m15 = 15;
}
