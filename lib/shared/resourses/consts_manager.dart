class ConstsManager{
  //timers
  static const int durationInMilliseconds = 500;
  static const int durationInMillisecondsInPreviewImages = 200;
  static const int durationInSecondsInterval = 3;
  static const int durationInSecondsSnackBar = 4;
  static const int durationInMilliseconds3 = 100;
  static const int durationInMilliseconds4 = 400;
  static const int durationInSecondsCarouselAnimation = 1;
  static const int durationInSecondLottieAnimation = 2;
  static const int durationInSecondsSplashScreen = 3;
  static const int durationInMillisecondsToast = 450;
  static const int durationInMillisecondsBackRouting = 300;

  //language
  static const String en = "en";
  static const String ar = "ar";

  /// tables
  static const String tableFavorites = 'shop_favs';
  static const String tableCart = 'shop_cart';
}