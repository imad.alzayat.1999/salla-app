import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../fonts_manager.dart';

TextStyle getTextStyleBaloo2(
  double fontSize,
  FontWeight fontWeight,
  Color textColor,
  TextDecoration decoration,
  TextOverflow textOverflow,
) {
  return TextStyle(
    color: textColor,
    fontFamily: FontFamilyManager.fontFamilyBaloo2,
    fontWeight: fontWeight,
    fontSize: FontSizeManager.getFontSize(fontSize),
    decoration: decoration,
    overflow: textOverflow,
  );
}

//regular style
TextStyle getRegularTextStyleBaloo2({
  double fontSize = FontSizeManager.size12,
  required Color textColor,
  TextDecoration textDecoration = TextDecoration.none,
  TextOverflow textOverflow = TextOverflow.visible
}) {
  return getTextStyleBaloo2(
    fontSize,
    FontWeightManager.regularWeight,
    textColor,
    textDecoration,
    textOverflow
  );
}

//medium style
TextStyle getMediumTextStyleBaloo2({
  double fontSize = FontSizeManager.size12,
  required Color textColor,
  TextDecoration textDecoration = TextDecoration.none,
  TextOverflow textOverflow = TextOverflow.visible
}) {
  return getTextStyleBaloo2(
    fontSize,
    FontWeightManager.mediumWeight,
    textColor,
    textDecoration,
    textOverflow
  );
}

//light style
TextStyle getLightTextStyleBaloo2({
  double fontSize = FontSizeManager.size12,
  required Color textColor,
  TextDecoration textDecoration = TextDecoration.none,
  TextOverflow textOverflow = TextOverflow.visible
}) {
  return getTextStyleBaloo2(
    fontSize,
    FontWeightManager.lightWeight,
    textColor,
    textDecoration,
    textOverflow
  );
}

//semi bold style
TextStyle getSemiBoldTextStyleBaloo2({
  double fontSize = FontSizeManager.size12,
  required Color textColor,
  TextDecoration textDecoration = TextDecoration.none,
  TextOverflow textOverflow = TextOverflow.visible
}) {
  return getTextStyleBaloo2(
    fontSize,
    FontWeightManager.semiBoldWeight,
    textColor,
    textDecoration,
    textOverflow
  );
}

//bold style
TextStyle getBoldTextStyleBaloo2({
  double fontSize = FontSizeManager.size12,
  required Color textColor,
  TextDecoration textDecoration = TextDecoration.none,
  TextOverflow textOverflow = TextOverflow.visible
}) {
  return getTextStyleBaloo2(
    fontSize,
    FontWeightManager.boldWeight,
    textColor,
    textDecoration,
    textOverflow
  );
}
