import 'package:flutter/material.dart';

import '../fonts_manager.dart';

TextStyle getTextStyleTajawal(double fontSize, FontWeight fontWeight,
    Color textColor, TextDecoration decoration, TextOverflow textOverflow) {
  return TextStyle(
      color: textColor,
      fontFamily: FontFamilyManager.fontFamilyTajawal,
      fontWeight: fontWeight,
      fontSize: FontSizeManager.getFontSize(fontSize),
      decoration: decoration,
      overflow: textOverflow);
}

//regular style
TextStyle getRegularTextStyleTajawal(
    {double fontSize = FontSizeManager.size12,
    required Color textColor,
    TextDecoration textDecoration = TextDecoration.none,
    TextOverflow textOverflow = TextOverflow.visible}) {
  return getTextStyleTajawal(fontSize, FontWeightManager.regularWeight,
      textColor, textDecoration, textOverflow);
}

//medium style
TextStyle getMediumTextStyleTajawal(
    {double fontSize = FontSizeManager.size12,
    required Color textColor,
    TextDecoration textDecoration = TextDecoration.none,
    TextOverflow textOverflow = TextOverflow.visible}) {
  return getTextStyleTajawal(fontSize, FontWeightManager.mediumWeight,
      textColor, textDecoration, textOverflow);
}

//light style
TextStyle getLightTextStyleTajawal(
    {double fontSize = FontSizeManager.size12,
    required Color textColor,
    TextDecoration textDecoration = TextDecoration.none,
    TextOverflow textOverflow = TextOverflow.visible}) {
  return getTextStyleTajawal(fontSize, FontWeightManager.lightWeight, textColor,
      textDecoration, textOverflow);
}

//semi bold style
TextStyle getSemiBoldTextStyleTajawal(
    {double fontSize = FontSizeManager.size12,
    required Color textColor,
    TextDecoration textDecoration = TextDecoration.none,
    TextOverflow textOverflow = TextOverflow.ellipsis}) {
  return getTextStyleTajawal(fontSize, FontWeightManager.semiBoldWeight,
      textColor, textDecoration, textOverflow);
}

//bold style
TextStyle getBoldTextStyleTajawal({
  double fontSize = FontSizeManager.size12,
  required Color textColor,
  TextDecoration textDecoration = TextDecoration.none,
  TextOverflow textOverflow = TextOverflow.visible
}) {
  return getTextStyleTajawal(
      fontSize, FontWeightManager.boldWeight, textColor, textDecoration , textOverflow);
}
