import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FontFamilyManager {
  static const String fontFamilyBaloo2 = "Baloo2";
  static const String fontFamilyTajawal = "Tajawal";
}

class FontWeightManager {
  static const FontWeight lightWeight = FontWeight.w300;
  static const FontWeight regularWeight = FontWeight.w400;
  static const FontWeight mediumWeight = FontWeight.w500;
  static const FontWeight semiBoldWeight = FontWeight.w600;
  static const FontWeight boldWeight = FontWeight.w700;
}

class FontSizeManager {
  static const double size10 = 10;
  static const double size11 = 11;
  static const double size12 = 12;
  static const double size13 = 13;
  static const double size14 = 14;
  static const double size15 = 15;
  static const double size16 = 16;
  static const double size17 = 17;
  static const double size18 = 18;
  static const double size19 = 19;
  static const double size20 = 20;
  static const double size21 = 21;
  static const double size22 = 22;
  static const double size23 = 23;
  static const double size24 = 24;

  static double getFontSize(double fontSize){
    return fontSize.sp;
  }
}
