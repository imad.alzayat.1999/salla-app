class RoutesManager{
  static const String cartRoute = "/cart";
  static const String categoriesRoute = "/categories";
  static const String changePasswordRoute = "/change_password";
  static const String contactInfoRoute = "/contact_info";
  static const String detailsRoute = "/details";
  static const String faqRoute = "/faq";
  static const String profileInfoRoute = "/profile_info";
  static const String searchRoute = "/search";
  static const String settingsRoute = "/settings";
  static const String favoritesRoute = "/favorites";
  static const String languagesRoute = "/languages";
  static const String loginRoute = "/login";
  static const String notificationsRoute = "/notifications";
  static const String onBoardRoute = "/on_board";
  static const String homeRoute = "/home";
  static const String profileRoute = "/profile";
  static const String selectLanguageRoute = "/select_language";
  static const String signupRoute = "/signup";
  static const String splashRoute = "/splash";
}

