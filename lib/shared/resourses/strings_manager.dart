class StringsManager {
  static String cartString = "Cart";
  static String categoryString = "Categories";
  static String contactsString = "Contacts";
  static String settingsString = "Settings";
  static String languageString = "Language";
  static String detailsDescriptionString = "Description: ";
  static String detailsPricingsString = "Pricings: ";
  static String addToCartString = "Add To Cart";
  static String faqString = "FAQ";
  static String favoritesString = "Favorites";
  static String discountString = 'DISCOUNT';
  static String notificationsString = "Notifications";
  static String emptyCartString = "Empty Cart";
  static String walletString = "Wallet";
  static String firstQuestionString = "do you have an empty cart?";
  static String secondQuestionString = "do you have a full wallet?";
  static String skipString = "Skip";
  static String emailString = "Email: ";
  static String passwordString = "Password: ";
  static String nameString = "Name: ";
  static String phoneString = "Phone: ";
  static String registerString = "Signup";
  static String productsString = 'Products';
  static String homePageString = "Home Page";
  static String profileString = "Profile";
  static String logoutString = 'Logout';
  static String updateString = "Update";
  static String registerQuestionString = "Don't you have an account ???";
  static String loginString = "Login";
  static String validationEmailString = "Please enter a valid email";
  static String validationPassString = "Please enter a valid password";
  static String validationNameString = "Please enter a valid name";
  static String validationPhoneString = "Please enter a valid phone number";
  static String errorString = "Error";
  static String errorChangePasswordString =
      "an error occurred when changing password";
  static String errorAddToFavoritesString = "an error occurred while adding";
  static String errorFetchFavoritesString = "an error occurred while fetching";
  static String errorDeleteFromFavoritesString =
      "an error occurred while deleting";
  static String errorUpdateProductInCartString = "an error occurred while updating";
  static String successString = "Success";
  static String successAddToCart =
      "Product has been added to cart successfully";
  static String successEditProfileString = "Profile has been edited successfully";
  static String infoString = "Info";
  static String infoQuantityNotLessThanOneString =
      "The quantity shouldn't be less than one";
  static String successDeleteProductFromCart =
      "Product has been deleted from cart successfully";
  static String successDeleteProductFromFavorites =
      "Product has been deleted from favorites successfully";
  static String successUpdateProductSuccessfully =
      "Product has been updated successfully";
  static String successAddToFavoritesString = "Added to favorites Successfully";
  static String warningString = "Warning";
  static String productInCartString = "Product In Cart";
  static String productInFavoritesString = "Product In Favorites";
  static String nothingFoundString = "Nothing Found";
  static String englishTitleValueString = "English";
  static String arabicTitleValueString = "Arabic";
  static String goToOnBoardString = "Go to onboard screen";
  static String changePasswordString = "Change Password";
  static String currentPasswordString = "Current Password";
  static String newPasswordString = "New Password";
  static String moveToTrashString = "Move To Trash";
  static String deleteConfirmationString = "Delete Confirmation";
  static String areYouSureString = "Are you sure you want to delete this item?";
  static String deleteString = "Delete";
  static String cancelString = "Cancel";
  static String profileInfoString = "Profile Info";
  static String profileEditString = "Edit Profile";
  static String subtotalString = "Subtotal:";
  static String totalPriceString = "Total Price: ";
  static String totalQuantityString = "Total Quantity: ";
  static String searchHereString = "Search Here";
}
