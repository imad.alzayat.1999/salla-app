class IconManager {
  static const String iconsPath = "assets/icons";

  //icons
  static const String failedIcon = "$iconsPath/failed.png";
  static const String plusIcon = "$iconsPath/plus.png";
  static const String minusIcon = "$iconsPath/minus.png";
  static const String trashIcon = "$iconsPath/trash.png";
  static const String dollarIcon = "$iconsPath/dollar.png";
  static const String skipIcon = "$iconsPath/skip.png";
  static const String faqIcon = "$iconsPath/faq.png";
  static const String languageIcon = "$iconsPath/language.png";
  static const String contactsIcon = "$iconsPath/contact.png";
  static const String emailIcon = "$iconsPath/email.png";
  static const String passwordIcon = "$iconsPath/password.png";
  static const String userIcon = "$iconsPath/user.png";
  static const String phoneIcon = "$iconsPath/iphone.png";
  static const String favoriteIcon = "$iconsPath/favorite.png";
  static const String cartIcon = "$iconsPath/cart.png";
  static const String productsIcon = "$iconsPath/products.png";
  static const String categoryIcon = "$iconsPath/category.png";
  static const String profileIcon = "$iconsPath/profile.png";
  static const String settingsIcon = "$iconsPath/settings.png";
  static const String logoutIcon = "$iconsPath/logout.png";
  static const String notificationIcon = "$iconsPath/notification.png";
  static const String upArrowIcon = "$iconsPath/up-arrow.png";
  static const String downArrowIcon = "$iconsPath/down-arrow.png";
  static const String checkMarkIcon = "$iconsPath/check-mark.png";
  static const String searchIcon = "$iconsPath/search.png";
  static const String cameraIcon = "$iconsPath/camera.png";
  static const String addToCartIcon = "$iconsPath/add_to_cart.png";
  static const String addedToCartIcon = "$iconsPath/added_to_cart.png";
  static const String infoIcon = "$iconsPath/info.png";
  static const String warningIcon = "$iconsPath/warning.png";
  static const String doneIcon = "$iconsPath/tick.png";
  static const String englishIcon = "$iconsPath/english.png";
  static const String arabicIcon = "$iconsPath/arabic.png";
  static const String changePasswordIcon = "$iconsPath/synchronize.png";
  static const String facebookIcon = "$iconsPath/facebook.png";
  static const String instagramIcon = "$iconsPath/instagram.png";
  static const String twitterIcon = "$iconsPath/twitter.png";
  static const String whatsappIcon = "$iconsPath/whatsapp.png";
  static const String snapchatIcon = '$iconsPath/snapchat.png';
  static const String youtubeIcon = "$iconsPath/youtube.png";
  static const String worldWideWebIcon = "$iconsPath/world-wide-web.png";
  static const String penIcon = "$iconsPath/pen.png";
  static const String deleteFavoritesIcon = "$iconsPath/delete_favorites.png";
  static const String leftArrowIcon = "$iconsPath/left-arrow.png";
  static const String pointIcon = "$iconsPath/point.png";
}

class ImageManager {
  static const String imagePath = "assets/images";

  //images
  static const String marketImage = "$imagePath/market.svg";
  static const String walletImage = "$imagePath/wallet.svg";
  static const String shoppingLogo = "$imagePath/splash_logo.png";
  static const String appLogo = "$imagePath/app_logo.png";

  //network image
  static const String avatarImage =
      "https://cdn.pixabay.com/photo/2018/08/28/13/29/avatar-3637561_960_720.png";
}

class LottieManager {
  static const String lottiePath = "assets/lottie";

  //lottie files
  static const String emptyLottie = "$lottiePath/empty.json";
  static const String noResultsLottie = "$lottiePath/no_results.json";
  static const String searchLottie = "$lottiePath/search.json";
}
