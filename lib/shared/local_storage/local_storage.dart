import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  static SharedPreferences? sharedPreferences;

  static init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static Future<bool> putData({required String key , required String value}) async{
    return await sharedPreferences!.setString(key, value);
  }

  static Future<bool> putBoolData({required String key , required bool value}) async{
    return await sharedPreferences!.setBool(key, value);
  }

  static String? getData({required String key}){
    return sharedPreferences!.getString(key);
  }

  static bool? getBool({required String key}){
    return sharedPreferences!.getBool(key);
  }

  static Future<bool> removeData({required String key}){
    return sharedPreferences!.remove(key);
  }
}
