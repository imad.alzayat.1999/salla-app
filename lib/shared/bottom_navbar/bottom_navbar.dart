import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/categories_module/views/categories_screen.dart';
import 'package:shop_application/modules/products_module/views/products_screen.dart';
import 'package:shop_application/modules/profile_info_screen/view/profile_info_screen.dart';
import 'package:shop_application/shared/resourses/assets_manager.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'bottom_navbar_item.dart';

class BottomNavbar extends StatefulWidget {
  final bool isProductsActivated;
  final bool isCategoryActivated;
  final bool isProfileActivated;
  final String language;

  const BottomNavbar({
    required this.isCategoryActivated,
    required this.isProductsActivated,
    required this.isProfileActivated,
    required this.language,
  });

  @override
  _BottomNavbarState createState() => _BottomNavbarState();
}

class _BottomNavbarState extends State<BottomNavbar> {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      elevation: SizeManager.s0,
      color: ColorsManager.kBottomNavbarColor,
      shape: const CircularNotchedRectangle(),
      child: Container(
        padding: const EdgeInsets.only(
          left: PaddingManager.p18,
          right: PaddingManager.p10,
          top: PaddingManager.p10,
        ),
        height: getProportionateScreenHeight(SizeManager.s60),
        child: Row(
          children: [
            BottomNavbarItem(
              iconData: IconManager.productsIcon,
              language: widget.language,
              onPressFunction: () => Get.to(ProductScreen()),
              isActivated: widget.isProductsActivated,
              bottomNavTitle: StringsManager.productsString.tr,
            ),
            SizedBox(width: getProportionateScreenWidth(SizeManager.s35)),
            BottomNavbarItem(
              iconData: IconManager.categoryIcon,
              language: widget.language,
              onPressFunction: () => Get.to(CategoriesScreen()),
              isActivated: widget.isCategoryActivated,
              bottomNavTitle: StringsManager.categoryString.tr,
            ),
            SizedBox(width: getProportionateScreenWidth(SizeManager.s35)),
            BottomNavbarItem(
              iconData: IconManager.profileIcon,
              language: widget.language,
              onPressFunction: () => Get.to(ProfileInfoScreen()),
              isActivated: widget.isProfileActivated,
              bottomNavTitle: StringsManager.profileString.tr,
            ),
          ],
        ),
      ),
    );
  }
}
