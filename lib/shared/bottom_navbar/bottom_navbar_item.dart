import 'package:flutter/material.dart';
import 'package:shop_application/shared/resourses/colors_manager.dart';
import 'package:shop_application/shared/resourses/consts_manager.dart';
import 'package:shop_application/shared/resourses/fonts_manager.dart';
import 'package:shop_application/shared/resourses/strings_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_baloo2_manager.dart';
import 'package:shop_application/shared/resourses/styles_manager/styles_tajawal_manager.dart';
import 'package:shop_application/shared/resourses/values_manager.dart';
import 'package:shop_application/shared/config/size_config.dart';
import 'package:shop_application/shared/widgets/icons/base_icon.dart';

class BottomNavbarItem extends StatelessWidget {
  final String iconData;
  final String language;
  final void Function()? onPressFunction;
  final bool isActivated;
  final String bottomNavTitle;

  const BottomNavbarItem(
      {required this.iconData,
      required this.language,
      required this.onPressFunction,
      required this.isActivated,
      required this.bottomNavTitle});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(SizeManager.s60),
      child: Material(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(SizeManager.s8),
        child: InkWell(
          borderRadius: BorderRadius.circular(SizeManager.s8),
          onTap: onPressFunction,
          child: Column(
            children: [
              BaseIcon(
                iconHeight: SizeManager.s16,
                iconPath: iconData,
                iconColor: isActivated
                    ? ColorsManager.kBlackColor
                    : ColorsManager.kWhiteColor,
              ),
              SizedBox(height: getProportionateScreenHeight(SizeManager.s5)),
              Text(
                bottomNavTitle,
                maxLines: SizeManager.s2.toInt(),
                textAlign: TextAlign.center,
                style: language == ConstsManager.en
                    ? getRegularTextStyleBaloo2(
                        fontSize: FontSizeManager.size10,
                        textColor: isActivated
                            ? ColorsManager.kBlackColor
                            : ColorsManager.kWhiteColor,
                        textOverflow: TextOverflow.ellipsis,
                      )
                    : getRegularTextStyleTajawal(
                        fontSize: FontSizeManager.size10,
                        textColor: isActivated
                            ? ColorsManager.kBlackColor
                            : ColorsManager.kWhiteColor,
                        textOverflow: TextOverflow.ellipsis,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
