import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shop_application/modules/splash_module/splash_screen.dart';
import 'package:shop_application/shared/bindings/all_bindings.dart';
import 'package:shop_application/shared/config/route_config.dart';
import 'package:shop_application/shared/language/controller/language_controller.dart';
import 'package:shop_application/shared/network/dio_helper.dart';
import 'package:shop_application/shared/language/global_translations.dart';
import 'package:shop_application/shared/local_storage/local_storage.dart';
import 'package:shop_application/shared/resourses/routes_mananger.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  DioHelper.init();
  await LocalStorage.init();
  runApp(
    Phoenix(child: MyApp()),
  );
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{
  LanguageController? languageController;

  @override
  void initState() {
    super.initState();
    languageController = Get.put(LanguageController());
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(390 , 844),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: GetMaterialApp(
            initialRoute: RoutesManager.splashRoute,
            initialBinding: AllBindings(),
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            onGenerateRoute: RouteConfig.onGenerateRoute,
            locale: Locale(languageController!.language),
            translations: GlobalTranslations(),
          ),
        );
      },
    );
  }
}

